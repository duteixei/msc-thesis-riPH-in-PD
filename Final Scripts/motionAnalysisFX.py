'''
Project: Computational models for robot-induced hallucinations in Parkinson's Disease
Laboratory of Cognitive Neuroscience - LNCO
Author: Duarte Rodrigues
Script: This document is a library with the functions developed to implement all the analysis pipeline.
In case there is a plotting option it is intended to be done in the Jupyter Notebook.
Outside modules, such as DLC Kinematics are used to help calculate features from the .h5 data files.

NOTE 1: Most of the functions for the single-animal DLC have been deprecated, and replaced by its multi-animal (ma) equivalent!

NOTE 2: Do not mix up single animal DLC / multi-animal DLC  (this changes how the movement data is loaded)  VS     study1 / study2  (changes how the psych. questionnaire data is loaded)
''' 

#Library import
import pandas as pd
import dlc2kinematics
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button
import numpy as np
from scipy import signal
from IPython.display import display
from scipy.signal import find_peaks
import math
import os
import re
    
def load_ma_datah5(h5, ma=True):
    """
    Load data from hdf5 file format and prepare it for processing step
    
    Parameters:
    h5 (str): file path to .h5 file
    ma (bool, optional): flag for multi animal or single animal data. Default is True.
    
    Returns:
    tuple: data, scorer, individuals, bodyparts (if ma=True) or data, scorer, bodyparts (if ma=False)
        data (pd.DataFrame): data from the .h5 file
        scorer (str): name of the scorer
        individuals (str): name of the individuals
        bodyparts (list): list of body parts in the data (only if ma=True)
    
    Example:
    >>> load_ma_datah5("results.h5", ma=True)
    (pd.DataFrame, 'scorer name', 'individuals name', ['bodypart 1', 'bodypart 2', ...])
    >>> load_ma_datah5("results.h5", ma=False) # Single animal project
    (pd.DataFrame, 'scorer name', ['bodypart 1', 'bodypart 2', ...])
    """
    
    # Read .h5 file using pandas
    df = pd.read_hdf(h5)
    
    # Create MultiIndex from the data frame
    mi = pd.MultiIndex.from_frame(df)
    
    # Get the scorer name
    scorer = mi.get_level_values(0).name[0]
    
    # Get the individuals name (only if ma=True)
    if ma:
        individuals = mi.get_level_values(0).name[1]
    else:
        individuals = None
    
    # Get the number of columns
    num_col = len(df.columns)
    
    # Get the body parts
    bodyparts = []
    for i in range(0,num_col,2): # 3 - results .h5 file
        bodyparts.append(mi.get_level_values(i).name[2])

    # Return the data based on if the DLC model is multi-animal
    if ma:
        return df, scorer, individuals, bodyparts
    else:
        df.columns = df.columns.droplevel(1) # makes it equal to a single animal project df
        return df, scorer, bodyparts

def correct_NAN(st_df, scorer, bodypart, parameter, individual=''):
    """
    The function correct_NAN takes in a pandas dataframe 'st_df', a string 'scorer' representing the scorer's name,
    a string 'bodypart' representing the body part being analyzed, a string 'parameter' representing the X or Y
    being analyzed, and an optional string 'individual' representing the participant being analyzed.
    The function returns a corrected standardized dataframe 'st_df' and a numpy array 'sign' (stands for signal) with NaN values removed and 
    corrected.

    Inputs:
    - st_df (pd.DataFrame): pandas dataframe containing the standardized data
    - scorer (str): string representing the scorer's name
    - bodypart (str): string representing the body part being analyzed
    - parameter (str): string representing the parameter being analyzed - X or Y
    - individual (str): string representing the participant being analyzed (optional) - in our case it is only 1

    Outputs:
    - st_df (pd.DataFrame): corrected dataframe with NaN values removed and replaced with the last prediction
    - sign (np.array): numpy array of the corrected data - signal

    Example:
    >>> st_df, sign = correct_NAN(st_df, "researcherDate(...)", "finger1", "x", "individual")
    """
    # code to extract the relevant data from the dataframe based on the input arguments
    if len(individual) == 0:
        sign = st_df[scorer][bodypart][parameter]
    else:
        sign = st_df[scorer][individual][bodypart][parameter]
    
    # code to trim the data in case it starts with NaN values
    if math.isnan(sign[0]):
        count = 0
        for i in range(len(sign)):
            if math.isnan(sign[i]):
                count = count + 1
            else:
                break
        begin_trim = range(count)
        st_df = st_df.drop(begin_trim)
        sign = sign[count:]

    # code to reset the indices of the dataframe and the numpy array
    st_df.reset_index(drop=True,inplace=True)
    sign.reset_index(drop=True,inplace=True)
    
    # code to replace NaN values in the numpy array with the last prediction
    for i in range(len(sign)):
        if math.isnan(sign[i]):
            sign[i] = sign[i-1]
            if len(individual) == 0:
                st_df.iloc[i][scorer, bodypart, parameter] = st_df[scorer][bodypart][parameter][i-1]
            else:
                st_df.iloc[i][scorer, individual,  bodypart, parameter] = st_df[scorer][individual][bodypart][parameter][i-1]

    return st_df, sign

def standard_metric(df):
    """
    Calculates the standard metric from the input dataframe (mean height of the robot base and the robot top) 
    for all the videos of all the participants (same robot) for the single-animal dataframe.

    Parameters:
    df (DataFrame): The dataframe that contains the values for the standard metric calculation.
    
    Returns:
    float: The calculated standard metric, which is calculated as the difference between the Y coordinate of the robot base
    and the Y coordinate of the robot top, resulting in the mean height of the robot in pixels for that specific recording.
    
    Example:
    standard_metric(df)
    # Output: 2.0
    """
    if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
        df.columns = df.columns.droplevel(1)
        
    # Get the name of the scorer column
    scorer=df.columns[0][0]
    
    # Get the values for robot top y coordinate
    robot_top_y=list(df[scorer]['robot_top']['y'])
    mean_top_y = sum(robot_top_y)/ len(robot_top_y)
    
    # Get the values for robot base y coordinate
    robot_base_y=list(df[scorer]['robot_base']['y'])
    mean_base_y = sum(robot_base_y)/ len(robot_base_y)
    
    # Calculate the standard metric
    std_met=mean_base_y-mean_top_y
    return std_met

def ma_standard_metric(df, scorer, individuals):
    """
    Calculate the standard metric (mean height of the robot base and the robot top) 
    for all the videos of all the participants (same robot) for the multi-animal dataframe.

    Parameters:
    df (pandas.DataFrame): A dataframe of the multi-indexed data.
    scorer (str): The name of the scorer.
    individuals (str): The name of the individual.

    Returns:
    float: The calculated standard metric, which is calculated as the difference between the Y coordinate of the robot base
    and the Y coordinate of the robot top, resulting in the mean height of the robot in pixels for that specific recording.
    
    Example:
    >>> ma_standard_metric(df, 'ResearcherDate(...)', 'Participant')
    0.5

    """
    # Calculate the mean height of the robot top
    _, sign_top = correct_NAN(df, scorer, 'robot_top', 'y', individual=individuals)
    robot_top_y = list(sign_top)
    mean_top_y = sum(robot_top_y) / len(robot_top_y)
    
    # Calculate the mean height of the robot base
    _, sign_base = correct_NAN(df, scorer, 'robot_base', 'y', individual=individuals)
    robot_base_y = list(sign_base)
    mean_base_y = sum(robot_base_y) / len(robot_base_y)
    
    # Calculate the standard metric
    std_met = mean_base_y - mean_top_y

    # Check if the standard metric is NaN and show a warning in case it is
    if math.isnan(std_met):
        print('THE STANDARD METRIC IS NAN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

    return std_met

def ma_standardize_df(df,bodyparts,scorer,individuals):
    """
    Standardize the given dataframe `df` using the standard metric value, for the multi-animal dataframe.
    
    Parameters:
    df (pandas.DataFrame): The dataframe to be standardized.
    bodyparts (list): A list of body parts in the dataframe.
    scorer (str): The scorer in the dataframe.
    individuals (str): The name of the individual.
    
    Returns:
    pandas.DataFrame: The standardized dataframe, based on the robot height.
    
    Example:
    df, scorer, individuals, bodyparts = load_ma_datah5('example.h5')
    st_df = ma_standardize_df(df, bodyparts, scorer, individuals)
    print(st_df)
    """
    
    print('The standardization of data will start! This process may take some time...')
    # the standard metric for that specific video is calculates
    sm=ma_standard_metric(df,scorer, individuals)
    
    # The entire dataframe is standardized with the previous value and converted to centimeters
    st_df = df*(22/sm) # this converts the st_df to real centimenters taking the real robot height (22 cm) into account
    
    for bp in bodyparts:
        for ind in range(df.shape[0]):
            st_df.iloc[ind][scorer, individuals , bp, 'likelihood'] = df.iloc[ind][scorer][individuals][bp]['likelihood']

    return st_df
    
def standardize_df(df, bodyparts, scorer):
    """
    A function to standardize the data in the dataFrame of the single-animmal DLC projects.

    Parameters:
    df (pandas DataFrame): The DataFrame to be standardized.
    bodyparts (list): A list of strings representing the body parts of interest.
    scorer (str): A string representing the scorer of interest.

    Returns:
    st_df (pandas DataFrame): The standardized DataFrame.

    Example:
    df = pd.read_csv("data.csv")
    bodyparts = ["head", "arm", "leg"]
    scorer = "scorer1"
    st_df = standardize_df(df, bodyparts, scorer)
    """

    print("The standardization of data will start! This process may take some time...")
    
    sm = standard_metric(df)
    st_df = df / sm

    for bp in bodyparts:
        for ind in range(df.shape[0]):
            st_df.iloc[ind][scorer][bp]["likelihood"] = df.iloc[ind][scorer][bp]["likelihood"]
    
    return st_df  
    
def plot(x, y, data_label='Data', x_label='', y_label='', grid=False):
    """
    This function is used to plot a line graph using the x and y data points provided.
    To easy up and hasten the plotting process ina  single line.
    
    Parameters:
    x (list or numpy array): A list or numpy array containing the x values.
    y (list or numpy array): A list or numpy array containing the y values.
    data_label (str, optional): A label identifying the data points that are plotted. Default is 'Data'.
    x_label (str, optional): The label for the x-axis. Default is an empty string.
    y_label (str, optional): The label for the y-axis. Default is an empty string.
    grid (bool, optional): A flag to indicate whether to show grid lines along the x-axis. Default is False.
    
    Returns:
    None (shows the image)
    
    Example:
    >>> x = [1, 2, 3, 4, 5]
    >>> y = [2, 4, 6, 8, 10]
    >>> plot(x, y, data_label='Line 1', x_label='X values', y_label='Y values', grid=True)
    
    This will display a line graph with the data points [(1, 2), (2, 4), (3, 6), (4, 8), (5, 10)], labeled as 'Line 1'. 
    The x-axis will be labeled as 'X values' and the y-axis will be labeled as 'Y values'. The graph will also display grid lines along the x-axis.
    """
    
    plt.plot(x, y, label=data_label)
    if grid:
        plt.grid(axis='x')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend()
    plt.show()

def sanity_check(df, scorer, fs):
    """
    This function performs two checks to make sure the data in 'df' is sane and that the tracking has the minimum requirements to be useful.
    The first check verifies the height of the robot (distance between robot top and robot base) across time. The second
    check verifies the variance of the top and bottom of the robot. It also makes plots to show it, making it easy to validate!

    Parameters:
    df (pandas dataframe): a multi-index dataframe that contains the data for the robot
    scorer (string): the name of the scorer
    fs (int): the sample rate in Hz - The frame rate of the video from which the tracking comes from.
    
    Returns:
    None (shows the plots)
    """
    # Reorganize dataframe - it becomes the same as the single animal project
    if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
        df.columns = df.columns.droplevel(1)

    # Calculate time
    time = df.index/fs

    # Calculate robot height
    top_y = list(df[scorer]['robot_top']['y'])
    base_y = list(df[scorer]['robot_base']['y'])
    distance = []
    for ind in range(df[scorer]['robot_top']['y'].size):
        dist = base_y[ind] - top_y[ind]
        distance.append(dist)
    robot_height = standard_metric(df)
    thr = [robot_height] * len(top_y)

    # Plot first sanity check
    # NOTE: In this plot the average height together with the height on every frame! The value should be a good representation of the general tracking (There shouldn't be any outliers)
    fig, axs = plt.subplots(2,1)
    plt.sca(axs[0])
    plt.title('Robot Height')
    plot(time, distance, data_label='Height across time')
    plot(time, thr, data_label='Average Height', x_label='Time', y_label='Distance (Pixels)')
    plt.ylim(0,150)

    # Calculate variance of top and bottom of the robot
    top_y = list(df[scorer]['robot_top']['y'])
    top_x = list(df[scorer]['robot_top']['x'])
    base_y = list(df[scorer]['robot_base']['y'])
    base_x = list(df[scorer]['robot_base']['x'])
    variance_top = []
    for ind in range(df[scorer]['robot_top']['y'].size-1):
        varia = np.sqrt(pow(top_x[ind]-top_x[ind+1],2) + pow(top_y[ind]-top_y[ind+1],2))
        variance_top.append(varia)
    variance_base = []
    for ind in range(df[scorer]['robot_top']['y'].size-1):
        varia = np.sqrt(pow(base_x[ind]-base_x[ind+1],2) + pow(base_y[ind]-base_y[ind+1],2))
        variance_base.append(varia)

    # Plot second sanity check
    # NOTE: Calculates and compares the variance of the robot height data.
    plt.sca(axs[1])
    plt.title('Prediction Variance')

    plot(time[1:], variance_top, data_label = 'Top Robot', x_label='Time', y_label='Variance (Pixels)')
    plot(time[1:], variance_base, data_label = 'Base Robot', x_label='Time', y_label='Variance (Pixels)')
    plt.ylim(-30,30)

    plt.show()

def LowPassFilter(sign, fs, highcut, order, application=''):
    """
    This function applies a low-pass filter to a 1-dimensional signal `sign` using a Butterworth filter. 
    The cutoff frequency of the filter is specified by `highcut` and the order of the filter is specified by `order`. 
    Everything above the highcut is attenuated.
    The function also has an optional parameter `application` which allows you to specify the y-axis label for the plots
    of the raw and filtered signals. If specified, the function will display the plots of the raw and filtered signals side-by-side. 

    Parameters:
    sign (list or numpy array): The 1-dimensional signal to be filtered.
    fs (float): The sample rate of the signal in Hz - Same frame rate as the video.
    highcut (float): The cutoff frequency of the filter in Hz - calculated based on the PSD.
    order (int): The order of the Butterworth filter - This filter was chosen since it has the least time distortion/phase shift.
    application (str, optional): The y-axis label for the plots of the raw and filtered signals. The default is an empty string.
    
    Returns:
    filt_signal (numpy array): The filtered signal.
    
    Examples:
    >>> LowPassFilter(signal, 100, 10, 3, 'Signal Value')
    The function will display two plots of the raw and filtered signals, side-by-side, with the y-axis labeled as 'Signal Value'
    """
    # Calculate the time values for each sample
    if type(sign) == list:
        length = len(sign)
    else:
        length = sign.shape[0]
    time= np.arange(0, length, 1) / fs

    # Calculate the Nyquist frequency and the normalized cutoff frequency
    nyq = 0.5 * fs
    high = highcut / nyq

    # Design the Butterworth filter and apply it to the signal
    b, a = signal.butter(order, high, 'lowpass', analog=False)
    filt_signal = signal.filtfilt(b, a, sign)
    
    # If the `application` parameter is specified, display the plots of the raw and filtered signals
    if application !=  '':
        fig, axs = plt.subplots(2, 1)
        plt.sca(axs[0])
        plot(time, sign, data_label='Raw data', x_label='Time (seconds)')
        plt.ylabel(application)
        plt.sca(axs[1])
        plot(time, filt_signal, data_label='Filtered data', x_label='Time (seconds)')
        plt.ylabel(application)
        plt.xticks(np.arange(0, int(length * (1 / fs)), 2))
        plt.legend()
        plt.show()

    return filt_signal

def HighPassFilter(sign,fs,lowcut,order,application=''):
    """
    This function filters the input signal with a high-pass filter. The filter is designed using the Butterworth method.
    If the `application` argument is provided, the raw and filtered signals are plotted for comparison.
    The cutoff frequency of the filter is specified by `lowcut` and the order of the filter is specified by `order`. 
    Everything lower than the lowcut is attenuated.
    
    Inputs:
        sign: list or numpy array,
            the input signal to be filtered.
        fs: float,
            the sampling rate of the input signal.
        lowcut: float,
            the lower cut-off frequency of the high-pass filter.
        order: int,
            the order of the Butterworth filter.
        application: string, optional,
            the application the filtered signal is used for. If provided, it will be used as the label for the y-axis in the plot.

    Returns:
        filt_signal: numpy array,
            the filtered signal.
        
    Example:
        fs = 200
        lowcut = 2
        order = 5
        application = 'ECG'
        signal = np.random.randn(1000)
        filtered_signal = HighPassFilter(signal, fs, lowcut, order, application)
    """
    
    if type(sign) == list:
        length = len(sign)
    else:
        length = sign.shape[0]

    # Calculate the Nyquist frequency and the normalized cutoff frequency
    time= np.arange(0,length,1) / fs
    nyq = 0.5 * fs
    low = lowcut/nyq

    # Design the Butterworth filter and apply it to the signal
    b,a = signal.butter(order, low,'highpass', analog=False)
    filt_signal = signal.filtfilt(b,a,sign)
    
    if application != '':
        # Plot the raw and filtered signals
        fig, axs = plt.subplots(2,1)
        plt.sca(axs[0])
        plot(time,sign, data_label='Raw data',x_label='Time (seconds)')
        plt.ylabel(application)
        plt.sca(axs[1])
        plot(time,filt_signal, data_label='Filtered data',x_label='Time (seconds)')
        plt.ylabel(application)
        plt.xticks(np.arange(0, int(length*(1/fs)), 2))
        plt.legend()
        plt.show()

    return filt_signal

def SavGolWindow(sign,fs,window,order):
    """
    Applies a Savitzky-Golay filter to a signal.
    A widget appears with a slider that is able to try various window sizes to see which one fits best.
    This script was based on an open access code and tried to be implemented in jupyter notebook, but it has some glitches on the interface.
    
    
    For the project itself, this filter was not very useful since the window size and overlap had to change between videos, 
    and it is complicated to calculate the optimal values for the filter to work properly at smoothing the body part movement.
    
    Parameters:
    sign (array-like): Input signal
    fs (float): Sampling frequency of the signal
    window (int): Size of the smoothing window (odd integer)
    order (int): Order of the polynomial used in the filtering.
    
    Returns:
    filt_signal (array-like): Filtered signal
    
    Note:
    This function also generates a plot of the raw and filtered signals, with a slider to interactively change the window size of the filter.
    """
    filt_signal = signal.savgol_filter(sign, window, order)

    if type(sign) == list:
        length = len(sign)
    else:
        length = sign.shape[0]

    time= np.arange(0,length,1) / fs

    # Plotting
    fig = plt.figure()
    ax = fig.subplots()
    p = ax.plot(time, sign, '-')
    p, = ax.plot(time, filt_signal, color='yellow')
    plt.subplots_adjust(bottom=0.25)
    plt.title("Choose a proper window to smooth the plot: ")
    # Defining the Slider button
    ax_slide = plt.axes([0.25, 0.1, 0.65, 0.03]) #xposition, yposition, width and height

    # Properties of the slider
    win_size = Slider(ax_slide, 'Window size', valmin=5, valmax=1001, valinit=window, valstep=6)

    def update(val):
        """
        Callback function for the slider, updates the filtered signal based on the selected window size.
        
        Parameters:
        val (float): Value of the slider
        
        Returns:
        None
        """
        global current_v
        current_v = int(win_size.val)
        new_y = signal.savgol_filter(sign, current_v, order)
        p.set_ydata(new_y)
        fig.canvas.draw() #redraw the figure

    win_size.on_changed(update)

    plt.show()

    return filt_signal

def PSD(sign, fs, plotting=True):
    """
    Compute the Power Spectral Density (PSD) of a signal and plot its frequency spectrum.

    Parameters:
    sign (array): The input signal data.
    fs (int): The sampling frequency of the input signal.
    plotting (bool): Whether to plot the frequency spectrum or not. Defaults to True.

    Returns:
    tuple: The PSD and frequency data for the signal's frequency spectrum.

    Example:
    >>> signal = [1, 2, 3, 4, 5, 6]
    >>> fs = 10
    >>> PSD(signal, fs)
    ([2.7, 2.7, 2.7], [0.0, 0.1, 0.2])
    """
    # Determine the length of the input signal
    if type(sign) == list:
        n = len(sign)
    else:
        n = sign.shape[0]

    # Calculate the time and sample intervals
    time = np.arange(0, n, 1) / fs
    dt = 1/fs

    # Compute the Fast Fourier Transform (FFT) and PSD of the signal
    fhat = np.fft.fft(sign, n)
    PSD = fhat * np.conj(fhat) / n
    freq = (1 / (dt * n)) * np.arange(n)

    # Determine the frequency range for plotting
    L = np.arange(1, np.floor(n/2), dtype='int')

    if plotting:
        fig, axs = plt.subplots(2, 1)
        
        # Plot the raw signal data
        plt.sca(axs[0])
        plot(time, sign, data_label='Raw data', x_label='Time (seconds)', grid=True)
        plt.xticks(np.arange(0, (n * dt), 2))
        plt.legend()

        # Plot the PSD of the signal
        plt.sca(axs[1])
        plot(freq[L], PSD[L], data_label='Raw PSD', x_label='Frequency (Hz)', y_label='Power')
        plt.xlim(freq[L[0]], freq[L[250]])
        plt.legend()

        plt.show()
        
    return PSD[L], freq[L]

def betweenFrames_distance(df, bodypart, scorer):
    """
    Computes the euclidean distance between the position of a body part between two consecutive frames.
    This useful to check if there is any noise causing random spiked in the tracking.

    Parameters:
    df (pandas DataFrame): The input dataframe
    bodypart (str): The name of the body part for which the distance is to be calculated
    scorer (str): The name of the scorer for which the distance is to be calculated

    Returns:
    list: A list of euclidean distances between consecutive frames

    """
    distance = []
    for i in range(df.shape[0]-1):
        # extract x and y coordinate for a particular body part
        x1 = df[scorer][bodypart]['x'][i]
        y1 = df[scorer][bodypart]['y'][i]
        x2 = df[scorer][bodypart]['x'][i+1]
        y2 = df[scorer][bodypart]['y'][i+1]

        # calculate euclidean distance between two consecutive frames
        dist_euc = np.sqrt(pow((x2-x1),2)+pow((y2-y1),2))
        distance.append(dist_euc)
    return distance

def speed(df, bodypart, scorer, fs):
    """
    Calculate speed of a specific bodypart. It takes into consideration both X and Y axis. Based on the DLC Kinematics moule.
    
    Parameters:
    df: pandas dataframe with x and y position information
    bodypart: string, name of the body part
    scorer: string, name of the scorer
    fs: float, sample rate in Hz
    
    Outputs:
    return: numpy array, raw speed of the body part
    """
    if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
        df.columns = df.columns.droplevel(1)

    df_speed = dlc2kinematics.compute_speed(df,bodyparts=[bodypart]) # the maDLC df is shaped to be as the single animal so that the DLC Kinematics functions can be used
    bp_speed = df_speed[scorer][bodypart]['speed']

    if type(bp_speed) == list:
        n = len(bp_speed)
    else:
        n = bp_speed.shape[0]
        
    # lp_speed=LowPassFilter(bp_speed,fs,highcut,order,application='')    

    # filt_speed = SavGolFilter(lp_speed,fs,100,2,application='Speed')

    # fig, axs = plt.subplots(2,1)
    # plt.subplots_adjust(left=0.1,
    #                 bottom=0.1, 
    #                 right=0.9, 
    #                 top=0.9, 
    #                 wspace=0.4, 
    #                 hspace=0.4)
    # plt.sca(axs[0])

    # plot(time, bp_speed, data_label='Raw Speed', x_label='Time (seconds)', y_label='Speed')
    
    # plt.sca(axs[1])

    # plot(time, filt_speed, data_label='Filtered Speed', x_label='Time (seconds)', y_label='Speed')

    # plt.show()    
        
    return bp_speed

def velocity(df, bodypart, scorer, parameter):
    """
    Compute the velocity of a body part. It is based on the DLC Kinematics. It gives information on both axis (X and Y) independently.
    For the project, the poking velocity is on the X axis (moving arm/body front and back), therefore only that is given as an output.

    Parameters:
    df (pandas DataFrame): DataFrame containing the coordinates of the body part.
    bodypart (str): Name of the body part to compute velocity for.
    scorer (str): Name of the scorer.
    parameter (str): Parameter to compute velocity for (e.g. "x", "y").

    Returns:
    pandas Series: Velocity of the specified parameter for the specified body part and scorer.

    """
    if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
        df.columns = df.columns.droplevel(1)
    df_vel = dlc2kinematics.compute_velocity(df,bodyparts=[bodypart]) # the maDLC df is shaped to be as the single animal so that the DLC Kinematics functions can be used
    vel = df_vel[scorer][bodypart][parameter] # velocity of the X component
    return vel

def accel(df, bodypart, scorer, parameter = ('x','y','likelihood')):
    """
    Compute the acceleration of a given body part.  It is based on the DLC Kinematics. It gives information on both axis (X and Y) independently.
    For the project, the poking acceleration is on the X axis (moving arm/body front and back), therefore only that is given as an output.
    This metric was discarded since the jitter noise is exponentially worse when the 2nd derivative of the signal is computed.
    
    Parameters:
    df (pandas DataFrame): The dataframe that holds the coordinate information for the body part.
    bodypart (str): The name of the body part.
    scorer (str): The name of the scorer who annotated the body part.
    parameter (tuple, optional): The components of acceleration to return, including 'x', 'y', and 'likelihood'. Default is ('x', 'y', 'likelihood').

    Returns:
    pandas DataFrame: The acceleration of the body part in x and y directions.

    """
    if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
        df.columns = df.columns.droplevel(1)
    df_acc = dlc2kinematics.compute_acceleration(df,bodyparts=[bodypart]) # the maDLC df is shaped to be as the single animal so that the DLC Kinematics functions can be used
    acc = df_acc[scorer][bodypart][parameter]
    return acc

def signaltonoise(Arr, axis=0, ddof=0):
    """
    Computes the signal-to-noise ratio of a signal. This metric was attempted to check if the tracking had a lot of jitter, 
    or if a threshold could be designed to distinguish a good VS bad trackig, but unfortunately it was not successful.
    
    Parameters:
    - Arr (array-like): Input array.
    - axis (int, optional): Axis along which the mean and standard deviation are computed. Default is 0.
    - ddof (int, optional): Delta degrees of freedom. Default is 0.
    
    Returns:
    - ndarray: Signal-to-noise ratio of the input array.
    """
    
    Arr = np.asanyarray(Arr)
    me = Arr.mean(axis)
    sd = Arr.std(axis=axis, ddof=ddof)
    return np.where(sd == 0, 0, me/sd)

def time_list(sign, fs):
    """
    Compute time array for given signal. This makes it easy to get the X-axis, in seconds, when plotting.

    Parameters:
    sign (array-like): Input signal 
    fs (float): Sampling rate

    Returns:
    time (numpy array): Time array calculated from input signal and sampling rate

    Example:
    >>> sign = [1, 2, 3, 4, 5]
    >>> fs = 10
    >>> time_list(sign, fs)
    array([0. , 0.1, 0.2, 0.3, 0.4])
    """
    
    if type(sign) == list:
        length = len(sign)
    else:
        length = sign.shape[0]
    time = np.arange(0, length, 1) / fs
    return time

def factorial(x):
    """This is a recursive function
    to find the factorial of an integer (parameter: x)"""

    if x == 1:
        return 1
    else:
        # recursive call to the function
        return (x * factorial(x-1))

def optimal_SavGolWindow(sign, order=2):
    """
    This function tries to compute the optimal window size for Savitzky-Golay filter. It is a good attempt but it does not work for all the partipants.

    For the project itself, this filter was not very useful since the window size and overlap had to change between videos, 
    and it is complicated to calculate the optimal values for the filter to work properly at smoothing the body part movement.
    
    Parameters:
    sign (array-like): Input signal to be filtered.
    order (int, optional): The order of the polynomial used to fit the samples. Default is 2.

    Returns:
    final (array): The filtered signal.
    win_opt (int): The optimal window size.
    
    Example:
    >>>sign = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    >>>final, win_opt = optimal_SavGolWindow(sign)
    >>>print(final)
    [1.0878819 2.000398  2.9123875 3.7641634 4.5888397 5.3714456 6.1655519 6.9761125 7.8245795 8.6727097]
    >>>print(win_opt)
    51
    """
    if type(sign) == list:
        length = len(sign)
    else:
        length = sign.shape[0]
    
    #This window and overlap size was obtained by trial and error and is not ideal for all the participants
    win_opt=101
    win1 = 51
    
    #Algorithm based on article
    while(win1 != win_opt):
        win1 = int(2 * np.floor(win_opt / 2))
        dx = 0.1

        y = signal.savgol_filter(sign, win1, order)
        diff_y = np.diff(y) / dx

        dy = signal.savgol_filter(diff_y, win1, order)
        Y = np.diff(dy, n=order + 2) / (dx) ** (order + 2)

        n1 = 2 * (order + 2) * (factorial(2 * order + 3)) ** 2
        d1 = (factorial(order + 1)) ** 2
        n2 = np.var(sign)
        c1 = np.mean(Y ** 2)
        
        # vn = (1/length)*sum()

        win_opt = ((n1 / d1) * (n2 / c1)) ** (1 / (2 * order + 5))
        
    final = signal.savgol_filter(sign, win_opt, order)
    return final, win_opt

def spike_removal(sign, plotting=False):
    """
    This function removes spikes from the input signal. In DLC, the tracking if suffers from a spike, it is shifted from the baseline 
    postion where the movement is to a shifted position for a couple of frames. In shifted position (while in the spike) tracking 
    countinues to be accurate.
    The way to correct it is then to detect the initial spike, save the amount of it, and when the tracking gets back to the correct 
    baseline place, the tracking position on the previous frames are corrected based on the saved value.

    Parameters:
    sign (list): the input signal
    plotting (bool, optional): if True, the original and corrected signals are plotted (default is False)

    Returns:
    list: the corrected signal without spikes
    """
    x_mov = list(sign)
    
    # Plotting the signals if plotting is True
    if plotting:
        fig, axs = plt.subplots(2,1)
        plt.sca(axs[0])
        t= time_list(x_mov,60)
        plt.plot(t, x_mov, label='Missing data filled')
        plt.legend()
        plt.title('Spike correction')
        plt.ylabel('Distance to left border (centimeters)')
        plt.xlabel('Time (seconds)')

        plt.sca(axs[1])
        plt.plot(t, x_mov, label='Raw signal')

    # Window parameters disvovered by trial and error! They are not optimal for every participant, but works most of the time
    w_size = 280
    w_step = 230
    windows=range(0, len(x_mov)-w_size, w_step) 
    spike_liX=[]
    spike_liY=[]
    
    # Loop through each window
    for i in range(len(windows)):
        signal_cut = list(x_mov[windows[i]:windows[i]+w_size]) # 20% overlap

        # Threshold calculation
        cut_max = max(signal_cut)
        cut_min = min(signal_cut)
        amp_cut =cut_max-cut_min
        threshold = 0.25 * amp_cut

        first_spike = False
        # Loop through each sample in the window
        for idx in range(len(signal_cut)-1):
            diff = signal_cut[idx]-signal_cut[idx+1] # when the spike goes down, the diff is + (>0)
            if abs(diff) > threshold and first_spike == False:
                # There is a spike
                idx_A = idx
                diff_A = diff
                first_spike = True
                spike_liX.append(windows[i]+idx)
                spike_liY.append(x_mov[windows[i]+idx])
            elif abs(diff) > threshold and first_spike == True:
                if (diff_A > 0 and diff < 0) or (diff_A < 0 and diff > 0):
                    # Spike is finished
                    idx_B = idx
                    spike_liX.append(windows[i]+idx)
                    spike_liY.append(x_mov[windows[i]+idx])
                    # Correction of the spike
                    for s in range(idx_A+1,idx_B+1):
                        signal_cut[s]=signal_cut[s] + diff_A
                    first_spike=False

                if abs(diff+diff_A) > threshold: # in case it overshoots, meanig 2 spikes happened on the same frame. The first spike is corrected and the value of the new spike is saved.
                    idx_A = idx
                    diff_A = diff+diff_A
                    first_spike = True


        for r in range(windows[i],windows[i]+w_size):
            x_mov[r]=signal_cut[r-windows[i]]
            
    # Plotting the corrected signal - It will not be perfect, but it will be better than the original
    if plotting:
        
        plt.plot(t,x_mov,color='red', label='Spike corrected')
        # plt.scatter(spike_liX, spike_liY, color='green')
        plt.legend()
        plt.title('Spike correction')
        plt.ylabel('Distance to left border (centimeters)')
        plt.xlabel('Time (seconds)')
        plt.show()

    # Return the corrected signal
    return x_mov

def noise_metric(sign, fs):
    """
    Calculates the noise metric of a signal. A highpass filter is done to isolate the jitter noise. With this, then we calculate the maximum amplitude.
    This is a very simple metric that does not give much information. However, having the noise isolated is a good start to compute other things.

    Parameters:
    sign (list): List of the signal values
    fs (int): Sampling frequency of the signal in Hz

    Returns:
    float: The noise metric of the signal
    """
    # Apply a high-pass filter to the signal
    filt = HighPassFilter(sign, fs, 1.5, 5, application='')

    # Calculate the amplitude of the high frequency component of the signal
    max_filt = max(filt)
    min_filt = min(filt)
    noise_amp = max_filt - min_filt

    return noise_amp

def standardized_exists(path):
    """
    This function checks if a file with the word "standardized" exists in the given directory path.

    Parameters
    ----------
    path : str
        The path to the directory to be searched.

    Returns
    -------
    st_exist : bool
        Boolean indicating if a file with the word "standardized" exists in the directory.
    """

    # List all the files in the given directory
    file_list = os.listdir(path)
    
    # Flag to indicate if a file with the word "standardized" exists
    st_exist = False
    
    # Loop through the list of files
    for f in file_list:
        # Check if the word "standardized" is in the file name
        if 'standardized' in f:
            st_exist = True
    
    return st_exist

def all_nan(sign):
    """
    Check if the given signal contains all NaN values.

    Parameters:
    sign (list): A list of numerical values representing the signal

    Returns:
    bool: True if all values in the signal are NaN, False otherwise
    """
    occurences_nan = 0
    all_nan = False
    for i in range(len(sign)):
        if math.isnan(sign[i]):
            occurences_nan = occurences_nan + 1
    
    # Check if the number of NaN values in the signal is equal or greater than the length of the signal minus 10
    if occurences_nan >= len(sign) - 10: 
        all_nan = True
    
    return all_nan

def total_jitter_metric(noisy_sign,filt_sign,plotting=False):
    """
    The objective of this function is to have another metric to identify if the tracking is adequate.
    This function calculates the total jitter between two signals and plots the signals if requested.

    Parameters:
        noisy_sign (list): The noisy signal.
        filt_sign (list): The filtered signal.
        plotting (bool): A flag that indicates whether to plot the signals.

    Returns:
        avg_jitter (float): The average jitter between the two signals.
    """

    # Check if the two signals have the same length
    if len(noisy_sign) != len(filt_sign):
        print("The signals don't have the same size")
        # Cut the longer signal
        if len(noisy_sign) > len(filt_sign):
            extra = len(noisy_sign) - len(filt_sign)
            noisy_sign = noisy_sign[extra:]
        else:
            extra = abs(len(noisy_sign) - len(filt_sign))
            noisy_sign = filt_sign[extra:]
    
    # Calculate the sum of jitter
    sum_jitter = 0
    for i in range(len(noisy_sign)):
        sum_jitter = sum_jitter + abs(noisy_sign[i]-filt_sign[i])
    
    # Calculate the average jitter
    avg_jitter = sum_jitter/len(noisy_sign)

    # Plot the signals if requested
    if plotting:
        t = range(len(noisy_sign))
        plt.plot(t, noisy_sign)
        plt.plot(t, filt_sign)
        plt.show()

    return avg_jitter

def get_features(st_df, fs, scorer, bodypart, participant_name="", plotting="True", angle=""):
    """
    This function extracts several features from a two-dimensional signal, representing the x and y coordinates of a single body part.

    Args:
    st_df (pd.DataFrame): A pandas DataFrame that contains the data of a body part in the x and y axis.
    fs (int): The sample rate of the data.
    scorer (str): The name of the scorer who recorded the body part.
    bodypart (str): The name of the body part being analysed.
    participant_name (str, optional): The name of the participant whose data is being analyzed. Defaults to "".
    plotting (str, optional): Whether to plot the data or not. Defaults to "True".
    angle (str, optional): The angle of the body part being analysed, for example, LL (Left-Left). Defaults to "".

    Returns:
    list: A list of features that were calculated from the signal, including [Total jitter, Average velocity, Maximum velocity, Maximum Acceleration, Minimum Acceleration, RMS, and SDSD].
    
    Example:
    get_features(st_df, 100, 'Scorer1', 'finger1', 'Participant', 'False', 'LL')
    Returns: [0.005, 0.03, 0.05, 0.3, -0.3, 0.2, 0.1]
    """
    
    if pd.MultiIndex.from_frame(st_df).get_level_values(0).name[1] == 'participant':
        st_df.columns = st_df.columns.droplevel(1)
        
    feats = []

    # Trim the signal by 7 seconds on the beginning and end to make sure the data analysed is all of interest
    # Can be optimized with a participant-by-participant algortithm where the first and last poke are identified. Everything that is outside that interval is cropped
    
    total_dur = st_df.shape[0]/fs
    
    if total_dur > 20:
        trim_samples = int(7*fs)
        
        begin = range(trim_samples)
        end = range(st_df.shape[0]-trim_samples,st_df.shape[0])
        
        st_df = st_df.drop(end)
        st_df = st_df.drop(begin)
        st_df.reset_index(drop=True,inplace=True)
        
    # Movement in X axis

    if all_nan(st_df[scorer][bodypart]['x']):
        print('This ' + bodypart + ' was not tracked! All feats NAN.')
        nan_list=[math.nan,math.nan,math.nan,math.nan,math.nan,math.nan,math.nan]
        if 'finger' in bodypart:
            nan_list.append(math.nan)
        return nan_list

    # Clean, filter and process the data
    st_df, x_mov_noisy = correct_NAN(st_df, scorer, bodypart, 'x')
    noise = noise_metric(x_mov_noisy,fs)
    x_mov = spike_removal(x_mov_noisy,plotting=plotting)

    extra_cut = 1.5# I am adding 1.5 Hz more than the threshold to encompass other frequencies that may be important to have an accurate smoothed representation of the signal
    psd, freq = PSD(x_mov, fs,plotting=False)
    iii = np.where(psd == max(psd[20:])) # Finds frequencies with higher magnitudes

    if len(iii[0])==0:
        cutoff = extra_cut
    else:
        cutoff = freq[int(iii[0])] + extra_cut # what is the frequency most proeminent in the signal

        if cutoff < extra_cut-0.1: # In case something went wrong
            cutoff = extra_cut

    filt_mov_x = LowPassFilter(x_mov,fs,cutoff,5,application='') # Order 5 (value discovered by trial and error)

    jitter = total_jitter_metric(x_mov_noisy,filt_mov_x) # signal quality metric comparing the jitter between thw raw and filtered signal

    # if the video is filmed from the left the signal needs to be flipped
    if angle == 'LL' or angle == 'LD':
        filt_mov_x =  -filt_mov_x + (2*np.mean(filt_mov_x))

    # Initializing the new filtered DF to be used later to calculate the movement speed
    filt_df = st_df.copy()
    for ind in range(st_df.shape[0]):
        filt_df.iloc[ind][scorer, bodypart, 'x'] = filt_mov_x[ind]

    # Movement in Y axis (same process and functions as in X axis)
    
    # Clean, filter and process the data
    st_df, y_mov_noisy = correct_NAN(st_df, scorer, bodypart, 'y')
    y_mov = spike_removal(y_mov_noisy)
    
    psd, freq = PSD(y_mov, fs,plotting=False)
    iii = np.where(psd == max(psd[20:])) 

    if len(iii[0])==0:
        cutoff = extra_cut
    else:
        cutoff = freq[int(iii[0])] + extra_cut
    
        if cutoff < extra_cut-0.1:
            cutoff = extra_cut

    filt_mov_y = LowPassFilter(y_mov,fs,cutoff,5,application='')

    if angle == 'LL' or angle == 'LD':
        filt_mov_y =  -filt_mov_y + (2*np.mean(filt_mov_y))

    for ind in range(filt_df.shape[0]):
        filt_df.iloc[ind][scorer, bodypart, 'y'] = filt_mov_y[ind]


    # In order to study the movement more correctly, specially front and back, the wandering baseline needs to be removed. (the wandering is due to changes in behavior)
    # To this the signal is flattened by subtracting from the filtered signal a component that has very low frequencies (0.1 Hz)

    # Movement in X axis
    flat_si=[]
    low_filt_mov_x = LowPassFilter(x_mov,fs,0.1,5,application='') # Both the order and cutoff are static

    if angle == 'LL' or angle == 'LD':
        low_filt_mov_x =  -low_filt_mov_x + (2*np.mean(low_filt_mov_x))

    for i in range(len(x_mov)):
        # sub = x_mov[i]- low_filt_mov_x[i] # tried to flatten the raw signal, but it does not make good results
        sub = filt_mov_x[i] - low_filt_mov_x[i]
        flat_si.append(sub)
    flat_si = np.array(flat_si)
    
    # With the flat signal is possible to extract the amplitude of movement
    peaks, _ = find_peaks(flat_si, height=np.mean(flat_si), distance=180)
    valleys, _ = find_peaks(-flat_si, height=np.mean(-flat_si), distance=180)

    avg_peaks = np.mean(flat_si[peaks])
    avg_valleys = np.mean(flat_si[valleys])

    
    if plotting:

        plt.plot(flat_si)
        plt.plot(peaks, flat_si[peaks], "x")
        plt.plot(valleys, flat_si[valleys], "yo")

        if type(flat_si) == list:
            length = len(flat_si)
        else:
            length = flat_si.shape[0]

        # making lists to plot the average/mean values to see if they respect the signal
        thr=[np.mean(flat_si)]*length
        ap = [avg_peaks]*length
        av = [avg_valleys]*length
        plt.plot(thr, "--", color="gray")
        plt.plot(ap, "--", color="orange")
        plt.plot(av, "--", color="blue")
        plt.title(participant_name)
        plt.show()

    #Create feature vector

    # Basic statistical information regarding the original signal
    feats.append(np.mean(filt_mov_x))
    feats.append(np.std(filt_mov_x))

    # Information extracted based on the flattened signal
    x_mov_max = max(flat_si)
    x_mov_min = min(flat_si)
    x_mov_amp = x_mov_max - x_mov_min 
    feats.append(x_mov_amp) # maximum amplitude

    avg_amp = avg_peaks - avg_valleys
    feats.append(avg_amp) # average amplitude

    speed_ = list(speed(filt_df, bodypart, scorer, fs))
    feats.append(np.mean(speed_)) # Speed average
    feats.append(np.std(speed_)) # Speed standard deviation
    
    # Signal quality metrics
    feats.append(noise)
    feats.append(jitter)

    # it only makes sense to calcultae the poking frequency on the finger movement - added in the first position of the feature vector
    if 'finger' in bodypart: 

        zeroX_count = 0
        for i in range(len(flat_si)-1):
            if flat_si[i] < 0 and flat_si[i+1] > 0:
                zeroX_count = zeroX_count +1

            elif flat_si[i] > 0 and flat_si[i+1] < 0:
                zeroX_count = zeroX_count +1

        num_pokes = zeroX_count/2
        if type(flat_si) == list:
            length = len(flat_si)
        else:
            length = flat_si.shape[0]

        poking_freq = num_pokes/(length/fs)
        feats.insert(0, poking_freq)


    # Feature vector order: 

    # 0 - poking frequency

    # 1 - mean mov
    # 2 - std mov
    # 3 - max-amp
    # 4 - avg_amp
    # 5 - avg_speed
    # 6 - std_speed
    # 7 - max_jitter/high-freqnoise (in X)
    # 8 - total_jitter (in X)

    # Warning in case a feature is not calculated correctly
    for index in range(len(feats)):
        if np.isnan(feats[index]):
            print("Feature not correctly calculated")

    return feats

def move_left_to_right(df, scorer, finger, fs):
    """
    Since the video angle can only directly record the movement front/back and up/down a proxy was used to try to study the Right/Left movement
    using the robot logo.
    When the tip of the finger gets towards the left (when the arm is extended), the logo of the robot gets closer to the finger.
    
    NOTE: This measure is not exact nor fully correct because the angle of filming should be corrected/standardized.
    
    This function plots the movement of the hand from left to right, based on the logo indirect measure.

    Parameters:
    df (pd.DataFrame): DataFrame containing the data
    scorer (str): Column name of the scorer
    finger (str): Column name of the finger
    fs (int): Sampling frequency

    Returns:
    None (Plots the signal)
    """

    # If MultiIndex is set for columns, drop the level
    if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
        df.columns = df.columns.droplevel(1)

    # Get the x-coordinate values for logo and finger
    logo_x = list(df[scorer]['robot_logo']['x'])
    finger_x = list(df[scorer][finger]['x'])

    # Calculate the distance between the logo and finger
    dist = []
    for i in range(len(logo_x)):
        dist.append(logo_x[i]-finger_x[i]) # the smaller it gets the more the participant moves the hand to the left!

    # Get the time list
    time = time_list(dist,fs)

    # Plot the distance over time
    plot(time,dist,data_label='left move',x_label='Time (s)',y_label='distance',grid=False)
    plt.show()

def par_name_study1(path):
    """
    This function returns the participant name in Study 1 based on the file path.
    
    Args:
    path (str): The file path to extract the participant name from.
    
    Returns:
    str: The participant name.
    
    Example:
    >>> par_name_study1('/study1/data/P01_Test_Data')
    'P01'
    """
    # Get the base file name
    fi = os.path.basename(path)
    
    # Find the indices of underscores in the file name
    result = [_.start() for _ in re.finditer('_', fi)]
    
    # Extract the participant name
    par_name = fi[:2] + '_'+ fi[result[0]+1:result[1]]
    
    return par_name
    
def par_name(path):
    """
    Extract the participant name from the file path.
    
    Parameters:
        path (str): The file path to extract the participant name from.
        
    Returns:
        str: The participant name.
    """
    
    fi = os.path.basename(path)
    
    # On Study2, the participants' ID recorded in geneva start with 7. Based on that and on the rest of the configuration of the name, 
    # the ID is extracted.
    ind7 = fi.find('7')
    patient_name = fi[ind7:ind7+4]
    if patient_name[-1]=='_':
        patient_name = patient_name[:-1]
    elif patient_name[-1]=='R' or patient_name[-1]=='L':
        patient_name = patient_name[:-2]
    
    return patient_name

def get_par_list_with_PH(quest_path):
    """
    This function takes the path to a csv file containing participants' data and returns a list of participants who answered positively to the question "Have you ever had an experience with PH?"

    Parameters:
    quest_path (str): Path to the csv file containing participants' data

    Returns:
    list: A list of participants who answered positively to the question "Have you ever had an experience with PH?"

    """
    # Read the csv file into a Pandas dataframe
    df = pd.read_csv(quest_path)
    ph = []
    # Loop through the rows in the dataframe
    for i in range(df.shape[0]):
        # If the participant answered positively to the question, add their record_id to the list -> both PD and HC
        if int(df['robot_q3_ph'][i]) > 0 :
            ph.append(df['record_id'][i])  
    # Get the unique record_ids from the list, in case the same participant appears twice (both sync and async)
    uni_ph = np.unique(ph) 
    # Return the list of unique record_ids
    return uni_ph

def get_trial(quest_df, h5_path, cond):
    """
    This function returns the trial number for a given condition (sync or async) for a participant.
    The participant is identified by the file path of the .h5 file, and the condition information is obtained from the questionnaire data.
    
    Parameters:
    quest_df (pandas DataFrame) : DataFrame containing the questionnaire data.
    h5_path (str) : File path of the h5 file of the participant.
    cond (str) : The condition of interest ('sync' or 'async').

    Returns:
    trial (int) : Trial number (1 or 2) for the given condition.

    """

    sub = par_name(h5_path)
    list_ind=[]
    for i in range(quest_df.shape[0]):
        if quest_df['record_id'][i] == sub:
            list_ind.append(i)

    for i in range(quest_df.shape[0]):
        for ii in list_ind:
            if not math.isnan(quest_df['robot_exp_condition'][ii]) and not math.isnan(quest_df['rieq_stimulation_exp'][ii]):  # guarantee there is a value to be compared
                if quest_df['rieq_stimulation_exp'][ii] == 1:  # making sure it is the back experiment
                    
                    if quest_df['robot_exp_condition'][ii] == 1 and cond == 'sync': # sync is trial 1 (first to be done on the experiment)
                        trial = 1 
                        return trial
                    elif quest_df['robot_exp_condition'][ii] == 1 and cond == 'async': # sync is trial 1 (first to be done on the experiment) but I want async, so it was trial 2
                        trial = 2
                        return trial
                    elif quest_df['robot_exp_condition'][ii] == 2 and cond == 'async': # async is trial 1 (first to be done on the experiment)
                        trial = 1
                        return trial
                    elif quest_df['robot_exp_condition'][ii] == 2 and cond == 'sync': # async is trial 1 (first to be done on the experiment) but I want sync, so it was trial 2
                        trial = 2
                        return trial
                    
    # Return None if the trial could not be determined
    return None

def get_trial_study1(quest_df,h5_path,cond,hand):
    """
    This function returns the trial order for a given participant in the study 1 based on the conditions and hand information.

    Parameters:
    quest_df (pandas.DataFrame): The dataframe containing the questionnaire data.
    h5_path (str): The path to the h5 file for which the trial order needs to be found.
    cond (str): The condition to search for, either 'sync' or 'async'.
    hand (str): The hand to search for, either 'R' or 'L'.

    Returns:
    int: The trial order.
    """

    # Get the participant name from the h5 file path
    sub = par_name_study1(h5_path)
    
    # Store the last character of the participant name
    last_char=sub[-1]
    
    # Remove the underscores and zeros from the participant name
    sub=sub.replace('_','')
    sub=sub[:-1].replace('0','') # this becomes a problem if there is more than 100 participants
    sub=sub+last_char
    
    # Loop through the rows in the questionnaire dataframe
    for i in range(quest_df.shape[0]):
        # Check if the participant name, condition and hand match the values in the current row
        if quest_df['ID'][i] == sub and quest_df['Synchrony'][i] == cond and quest_df['Hand'][i] == hand:
            # If the values match, store the trial order for the current row and return it
            trial = quest_df['Order'][i]
            return trial

def get_PH_scale_study1(quest_df, h5_path, cond, hand):
    """
    Returns the participant's rating on the pleasantness-arousal scale (PH) in study 1.
    
    Parameters:
    quest_df (DataFrame): The DataFrame containing the questionnaires.
    h5_path (str): The path to the h5 file.
    cond (str): The condition ('sync' or 'async').
    hand (str): The hand ('left' or 'right').
    
    Returns:
    int: The participant's PH rating.
    """
    # Get the participant's name, removing the "_" and the leading zeros
    sub = par_name_study1(h5_path)
    last_char = sub[-1]
    sub = sub.replace("_", "")
    sub = sub[:-1].replace("0", "")
    sub = sub + last_char
    
    # Iterate over the rows of the questionnaires dataframe
    for i in range(quest_df.shape[0]):
        # Check if the current row is for the current participant, condition, and hand, and if the question is "PH"
        if quest_df["ID"][i] == sub and quest_df["Synchrony"][i] == cond and quest_df["Hand"][i] == hand and quest_df["Question"][i] == "PH":
            rating = quest_df["Rating"][i]
            return rating
                    
def get_PH_scale(quest_df,h5_path, cond):
    """
    Returns the PH scale value for the given condition in a specific .h5 data file.
    
    Parameters:
    quest_df (pandas DataFrame): DataFrame containing the questionnaire data.
    h5_path (str): The path of the h5 file.
    cond (str): The condition to look for, either 'sync' or 'async'.
    
    Returns:
    float: The PH scale value.
    
    """
    # Get the participant name from the h5 file
    sub = par_name(h5_path)
    
    # Create a list to store the index of the rows with the relevant participant name
    list_ind = []
    for i in range(quest_df.shape[0]):
        if quest_df['record_id'][i] == sub:
            list_ind.append(i)
            
    # Loop through the list of indices to find the PH scale value for the specified condition
    for i in range(quest_df.shape[0]):
        for ii in list_ind:
            # Check if there is a value for robot_exp_condition and rieq_stimulation_exp
            if not math.isnan(quest_df['robot_exp_condition'][ii]) and not math.isnan(quest_df['rieq_stimulation_exp'][ii]):
                # Check if the row corresponds to the back experiment
                if quest_df['rieq_stimulation_exp'][ii] == 1:
                    # Check if the condition is 'sync' and the robot_exp_condition is 1
                    if cond == 'sync' and quest_df['robot_exp_condition'][ii] == 1:
                        scale = quest_df['robot_q3_ph'][ii]
                        return scale
                    # Check if the condition is 'async' and the robot_exp_condition is 2
                    elif cond == 'async' and quest_df['robot_exp_condition'][ii] == 2:
                        scale = quest_df['robot_q3_ph'][ii]
                        return scale
       
def get_daily_PH_study1(quest_df, h5_path):
    """
    This function returns whether the  Parkinson's patients have daily presence hallucinations (PD-PH or PD-nPH) for a participant in study 1.

    Parameters:
    quest_df (pandas dataframe): dataframe containing the questionnaire data
    h5_path (str): file path of the h5 file of the participant

    Returns:
    dailyPH (str): does the participant has or not PH in daily life (PD-PH or PD-nPH)
    """
    # Get participant name from h5 file path
    sub = par_name_study1(h5_path)

    # Clean up participant name to match with the format in quest_df
    last_char = sub[-1]
    sub = sub.replace('_', '')
    sub = sub[:-1].replace('0', '')
    sub = sub + last_char
    
    # Loop through the rows of quest_df to find the participant with matching name
    for i in range(quest_df.shape[0]):
        if quest_df['ID'][i] == sub:
            # Get the dailyPH score for the participant
            dailyPH_score = quest_df['PH'][i]
            
            # Convert the dailyPH score to PD-PH or PD-nPH
            if dailyPH_score == 1:
                dailyPH = 'PD-PH'
            elif dailyPH_score == 0:
                dailyPH = 'PD-nPH'
                
            return dailyPH
      
def get_UPDS3_study1(quest_df, sub):
    """
    This function returns the UPDRS3-TOTAL score for a given subject in study 1.
    
    Parameters:
        quest_df (pd.DataFrame): A dataframe containing the questionaire data.
        sub (str): A string representing the subject name.
    
    Returns:
        UPDRS_score (float): The UPDRS3-TOTAL score for the subject.
    """
    
    #get proper participants ID
    last_char=sub[-1]
    sub=sub.replace('_','')
    sub=sub[:-1].replace('0','')
    sub=sub+last_char
    
    #collect the UPDRS3 total score from the questionnaire data file .csv
    for i in range(quest_df.shape[0]):
        if quest_df['ID'][i] == sub:
            UPDRS_score = quest_df['UDPRS3-TOTAL'][i]
            
            return UPDRS_score

def build_df_data(par,group,cond, trial, scale, dailyPH, features, hand='',angle=''):
    """
    The function 'build_df_data' creates a data frame with a set of variables. 

    Parameters:
    par (str): participant ID
    group (str): group of participant (e.g. PD, control)
    cond (str): condition (e.g. medicated, unmedicated)
    trial (str): trial number
    scale (float): PH scale score
    dailyPH (str): daily PH (e.g. 'PD-PH', 'PD-nPH')
    features (list of float): features of the movement data
    hand (str, optional): hand used for the movement task
    angle (str, optional): angle at which the movement was filmed

    Returns:
    pd.DataFrame: A data frame with the input variables

    """
    data_dict = dict()
    data_dict['sub_ID'] = par
    data_dict['group'] = group

    data_dict['condition'] = cond
    data_dict['trial'] = trial

    # check if the scale score is None, if so, fill with NaN
    if scale == None:
        not_in_quest= math.nan
        data_dict['PH_scale'] = not_in_quest
        data_dict['PH_Y_N'] = not_in_quest
    else:
        data_dict['PH_scale'] = scale

        # if the scale score is greater than 0, categorize as "robustly affected by PH"
        if scale > 0 :
            ph_bin = 'rob_ind_PH'
        else:
            ph_bin = 'not_rob_ind_PH'
        data_dict['PH_Y_N'] = ph_bin

    # check if the dailyPH is None, if so, fill with NaN
    if dailyPH == None:
        not_in_quest= math.nan
        data_dict['dailyPH_Y_N'] = not_in_quest
    else:
        data_dict['dailyPH_Y_N'] = dailyPH

    # check if hand is not an empty string, if so, add to data_dict
    if len(hand)!=0:
        data_dict['Hand_Used'] = hand
    
    # check if angle is not an empty string, if so, add to data_dict
    if len(angle)!=0:
        data_dict['Angle_Filmed'] = angle

    # check if the length of the features is correct, if not, print an error message
    if len(features)!=33 :
        print('The length of the features is wrong!!')

    # create the names for the different body parts and features
    bodyparts = ['finger_','wrist_', 'elbow_', 'shoulder_']
    feats = ['mean_mov','std_mov','max_amp','avg_amp','avg_speed','std_speed','Max_amp_jitter', 'Avg_Jitter']
    data_dict[bodyparts[0]+'freq_pokes'] = features[0]
    count=1
    for bp in bodyparts:
        for t in feats:
            data_dict[bp+t] = features[count]
            count+=1

    # create a dataframe from the data_dict
    data_df = pd.DataFrame(data_dict, index=[0])

    # return the data_df
    return data_df
    
def find_outliers_in_raw_detections(pickled_data, algo="uncertain", threshold=0.1, kept_keypoints=None):
    """
    Find outlier frames from the raw detections of multiple animal DLC.
    Can be a metric to evaluate how good a model is. 

    Parameter
    ----------
    pickled_data : dict
        Data in the *_full.pickle file obtained after `analyze_videos`.

    algo : string, optional (default="uncertain")
        Outlier detection algorithm. Currently, only 'uncertain' is supported
        for multi-animal raw detections.

    threshold: float, optional (default=0.1)
        Detection confidence threshold below which frames are flagged as
        containing outliers. Only considered if `algo`==`uncertain`.

    kept_keypoints : list, optional (default=None)
        Indices in the list of labeled body parts to be kept of the analysis.
        By default, all keypoints are used for outlier search.

    Returns
    -------
    candidates : list
        Indices of video frames containing potential outliers - By computing the length of the candidates list, it is possible to know how many frames have outliers
    """
    if algo != "uncertain":
        raise ValueError(f"Only method 'uncertain' is currently supported.")

    try:
        _ = pickled_data.pop("metadata")
    except KeyError:
        pass

    def get_frame_ind(s):
        return int(re.findall(r"\d+", s)[0])

    candidates = []
    data = dict()
    for frame_name, dict_ in pickled_data.items():
        frame_ind = get_frame_ind(frame_name)
        temp_coords = dict_["coordinates"][0]
        temp = dict_["confidence"]
        if kept_keypoints is not None:
            temp = [vals for i, vals in enumerate(temp) if i in kept_keypoints]
        coords = np.concatenate(temp_coords).squeeze()
        conf = np.concatenate(temp).squeeze()
        data[frame_ind] = np.c_[coords, conf]
        if np.any(conf < threshold):
            candidates.append(frame_ind)
    return candidates, data

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ FUNCTION TESTING ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


# get_features(st_df,30,scorer,'finger2')
# ==================================================================================================

# if type(x) == list:
#     length = len(x)
# else:
#     length = x.shape[0]

# x = speed(df, 'finger1', scorer, 30)
# plt.subplot(3,1,1)
# plt.plot(x)

# # u, win = optimal_SavGolWindow(x,order=2)
# # print(win)

# plt.subplot(3,1,2)
# #=========================================================================================================================
# #WORKS
# from scipy.signal import find_peaks

# peaks, _ = find_peaks(x, height=np.mean(x), distance=45)
# peaksd, _ = find_peaks(-x, height=np.mean(-x), distance=45)
# # peaksd, _ = find_peaks(x, distance=30)
# plt.plot(x)
# plt.plot(peaks, x[peaks], "x")
# plt.plot(peaksd, x[peaksd], "yo")

# thr=[np.mean(x)]*length
# plt.plot(thr, "--", color="gray")

# #=========================================================================================================================

# # plt.plot(u)

# plt.subplot(3,1,3)

# plt.plot(-x)
# plt.plot(peaksd, -x[peaksd], "go")
# thr=[np.mean(-x)]*length
# plt.plot(thr, "--", color="gray")
# # SavGolWindow(x,30,1001,2)

# plt.show()
# #=========================================================================================================================

# h5=r"D:\_users\Duarte_Projects\ma_FullModel-Duarte-2022-07-01\videos\Fullmodel_Results\HC_2_L_sync\HC_2_L_sync_DownCropDLC_dlcrnetms5_ma_FullModelJul1shuffle1_350000_el.h5"
# df,_,_,_ = load_ma_datah5(h5, ma=True)
# print(type(df))
# if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
#     print('participant level correctly identified')
#     df.columns = df.columns.droplevel(1)

# if pd.MultiIndex.from_frame(df).get_level_values(0).name[1] == 'participant':
#     print('the dropped df was activated even tho')
#     df.columns = df.columns.droplevel(1)
# print('this should be the secpnd message')
# display(df)

# #=========================================================================================================================

# h5 = r"D:\_users\Duarte_Projects\ma_FullModel-Duarte-2022-07-01\videos\Fullmodel_Results\HC_1_R_async\HC_1_R_async_DownCropDLC_dlcrnetms5_ma_FullModelJul1shuffle1_350000_el.h5"
# df, scorer, _ , _ = load_ma_datah5(h5, ma=True)
# fs = 100
# finger = 'finger1'
# move_left_to_right(df, scorer, finger, fs)
