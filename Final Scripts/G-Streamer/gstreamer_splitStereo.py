'''
Project: Computational models for robot-induced hallucinations in Parkinson's Disease
Laboratory of Cognitive Neuroscience - LNCO
Author: Duarte Rodrigues
Script: On study2 the video recordings are done with 2 cameras, and the videos from the right and left are stored
together, with the stereo view glued in a single file (split-screen).

For them to be analyzed by DLC they first need to be splitted. G_streamer is a great tool to this separation efficiently.
Here we insert the path to the entire database of videos. These are copied to the D:\, organized in the same way as it was on the Blanke-Lab,
and then in that same directory of the video, 2 new  videos are created labeled as "right_angle" and "left_angle" and the stereo is removed.

NOTE: The left_angle (right side in the video on stereo) is flipped horizontally for the front robot to stay on the left side of the frame, giving a more standardized look
to the database, giving lesser variation for the DLC to recognize it more easily.

NOTE: This file can only be compiled directly on the command line of MINGW/MSYS as shown in the G_streamer examples below.'''


import os
from moviepy.editor import *
import shutil

import gi
import sys
import traceback

gi.require_version("Gst", "1.0")
gi.require_version("GstApp", "1.0")

from gi.repository import Gst, GstApp, GLib

#=====================================================Gstreamer Init=========================================================================

_ = GstApp
Gst.init(sys.argv)

#=========================================================Gstreamer examples to base myself=====================================================================

# FOLLOW THIS STEPS TO RUN THIS SCRIPT
#cd d:/_users/Duarte_Projects/g_streamer_src
#python3 gstreamer_splitStereo.py

# Pay attention that in the path the orientation of the slash should be this /
# pt = "D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4"

# To get info
# gst-inspect-1.0.exe
# gst-inspect-1.0.exe nvh264enc

#To save 
# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! x264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov

# To display
#gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! videoconvert ! autovideosink


# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! videobox top=100 left=300 right=300 bottom=100 ! x264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov

# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! videobox top=100 left=300 right=300 bottom=100 ! videoscale ! video/x-raw, width=320, height=240 ! nvh264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov


#============================================================= Proper code =================================================================

def on_message(bus: Gst.Bus, message: Gst.Message, loop: GLib.MainLoop):
    mtype = message.type
    """ Necessary message and flag communication of G_Streamer to work on python.
        Gstreamer Message Types and how to parse
        https://lazka.github.io/pgi-docs/Gst-1.0/flags.html#Gst.MessageType
    """
    if mtype == Gst.MessageType.EOS:
        print("End of stream")
        loop.quit()

    elif mtype == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(err, debug)
        loop.quit()
    elif mtype == Gst.MessageType.WARNING:
        err, debug = message.parse_warning()
        print(err, debug)

    return True

def g_stream_split_stereo(videopath, videoname, dimensions, stereo_side=''):
    #============================================================= Gstreamer application =================================================================
    
    #Necessary correction for g_streamer to work
    gpath = videopath.replace('\\','/')
    save = os.path.dirname(videopath)
    gsave = save.replace('\\','/')

    print('G-STREAMER Start to split the stereo ' + videoname + ' !')
    # print(gpath)
    # print(gsave)
    
    # This is the actual g_streamer command pipeline to decode and parse the video, to then apply the changes and them rebuilding a new video with those changes
    # The left and right are the same - the only difference is that the left is flipped
    # the function doesn't return anything because the command line indicates were the new videos should be saved
    if stereo_side == 'left':
        pipeline = Gst.parse_launch("uridecodebin uri=file:///" + gpath + " ! queue ! videoconvert ! videobox top=" + str(int(0)) + " left=" + str(int(0)) + " right=" + str(int(dimensions[0]/2)) + " bottom=" + str(int(0)) + " ! nvh264enc ! h264parse ! qtmux ! filesink location=" + gsave + '/' + videoname + "_right_angle.mp4") #
    elif stereo_side == 'right':
        pipeline = Gst.parse_launch("uridecodebin uri=file:///" + gpath + " ! queue ! videoconvert ! videobox top=" + str(int(0)) + " left=" + str(int(dimensions[0]/2)) + " right=" + str(int(0)) + " bottom=" + str(int(0)) + " ! videoflip method=horizontal-flip ! videoconvert ! nvh264enc ! h264parse ! qtmux ! filesink location=" + gsave + '/' + videoname + "_left_angle.mp4") #
    else:
        print('Define the side of the video to cut from the stereo view.')

    # From here on out the rest of the function are the comands needed for python to integrate successfully with g_streamer

    bus = pipeline.get_bus()

    # allow bus to emit messages to main thread
    bus.add_signal_watch()

    # Start pipeline
    pipeline.set_state(Gst.State.PLAYING)

    # Init GObject loop to handle Gstreamer Bus Events
    loop = GLib.MainLoop()

    # Add handler to specific signal
    bus.connect("message", on_message, loop)

    try:
        loop.run()
    except Exception:
        traceback.print_exc()
        loop.quit()

    # Stop Pipeline
    pipeline.set_state(Gst.State.NULL)
    del pipeline

def exists_in_db(db_path, file):    
    ''' Evaluates if in the temporary directory (where the videos are stored after being splitted) exists a
    video with the same name as the input parameter 'file'.  
    In case it does, the code does not need to split that stereo again.'''
    
    for root, dirs, files in os.walk(db_path):
        for fil in files:
            if fil[:-15][-1] == '_':
                check = fil[:-16]
            else:
                check = fil[:-15]

            if check == file:
                return True

    return False  

# path to the original database of videos (in fullHD)
db_path = r'\\sv1arch\lnco-archives\usr\Duarte\PD-PH_normal'
temp_path=r'D:\_users\Duarte_Projects\SplitFlip'  # Folder on a local drive (D:\) -> The final splitted videos will be saved in here! it will be temporarily copied the original video here (needs memory)
db_ind = len(db_path)
for root, dirs, files in os.walk(db_path):
    for filenames in files:
        if 'stereo.mp4' in filenames: # detects all the videos that have the stereoview

            #videoname
            fi = filenames[1:-11]
            
            # if in the temporary directory doesn't exist this file, then do the split
            if not exists_in_db(temp_path, fi):
                
                #Piece of code to identify the participants name
                ind7 = fi.find('7')
                patient_name = fi[ind7:ind7+4]
                if patient_name[-1]=='_':
                    patient_name = patient_name[:-1]
                elif patient_name[-1]=='R' or patient_name[-1]=='L':
                    patient_name = patient_name[:-2]
                print(patient_name)
                print(root + '\\' + filenames)
                
                # Take out the intermideate 'Robot behavioural task' folder
                videopath_db = root + '\\' + filenames
                tree_path = videopath_db[db_ind:]
                index7 = tree_path.find('7')
                tree_path = tree_path[:index7+len(patient_name)+1]
                tree_path = tree_path.replace(' ','_') # gstreamer can't handle working on non-local drives (such as, the blanke-lab) and does not support white spaces in the paths
                save_path = temp_path+tree_path # replicates in the temporary folder the same configuration as it was in the blanke-lab
                if not os.path.exists(save_path):
                    os.makedirs(save_path)

                # copies the original video to the D:\, so then the g_streamer can do the splitting
                shutil.copy2(videopath_db,save_path)
                print('Copied to the D: Drive')
                videopath = save_path + os.path.basename(videopath_db)

                # Extracting the dimensions to know what/where the middle is
                clip = VideoFileClip(videopath)
                dimensions = clip.size
                clip.close()

                # Applying the g_streamer
                g_stream_split_stereo(videopath, fi, dimensions, stereo_side='left')
                g_stream_split_stereo(videopath, fi, dimensions, stereo_side='right') # On the right the view is horizontally flipped to uniformize the view, and to later on help on the extraction of features
                os.remove(videopath)
