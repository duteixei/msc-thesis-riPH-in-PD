'''
Project: Computational models for robot-induced hallucinations in Parkinson's Disease
Laboratory of Cognitive Neuroscience - LNCO
Author: Duarte Rodrigues
Script: This document should be run after the first tracking predictions were obtained from the simple downsampled resolution of the videos.
This is important because on study1 the camera sometimes moves or zooms while recording.
The point is to look to that predictions, and if they are good (likelihood > 0.8), determine what was the most extreme point left and right.

By respecting the aspect ratio of the video it is possible to isolate a region of interest, where the participant is, 
and consequently the cropping coordinate to achieve that area.

A series of corrections are applied, because cropping based on the aspect ratio can leave body parts out of frame.
The optimal set of cropping coordinates are then standardized for them to be 0 to 1.

In the end a text file is created, where each line has the information regarding a video for it to be cropped.
    videopath | optimal crop coordinates | videoname | aspect ratio | original dimensions

''' 

import os
from moviepy.editor import *
import dlc2kinematics


def search_db(path_db, videoname):
    ''' This function takes the path of the original database of videos (without being downcropped) and searches it to 
    find where the video with the same name is and returns the complete path of it.'''
    
    for root, dirs, files in os.walk(path_db):
        
        for filnam in files:
            
            if videoname in filnam:
                videopath = os.path.join(root,filnam)
                
                return videopath

def crop_coord(h5_path, aspect_ratio, dimensions):
    """
    Calculates the coordinates to crop a video to a specified aspect ratio and with a certain margin.
    It checks if the resulting cropping region covers the whole bodyparts.
    Made for the study1 in mind, when I tried to run it as a single animal DLC model.  It uses a set of initial predictions of DLC to make sure where the participant is located one the frame.
    
    Parameters:
    h5_path (str): path to the h5 file
    aspect_ratio (str): aspect ratio for cropping, the options are "16:9" or "4:3".
    dimensions (tuple): dimension of the video to be cropped in pixels, this is (width, height)
    
    Returns:
    coord (dict): A dictionary with the cropping coordinates in pixels, the keys are "top", "bottom", "left", "right"
    """
    # loading the prediction data of a condition
    df, bodyparts, scorer = dlc2kinematics.load_data(h5_path)

    # it's going through the all the bodyparts to check what was the most extreme point.
    for bp in bodyparts:

        # Getting the max and min points vertically
        y=list(df[scorer][bp]['y'])
        for i in range(len(y)):
            val = y[i]
            if i == 0:
                y_min = val
            elif val < y_min and df[scorer][bp]['likelihood'][i] > 0.8:
                y_min = val
            
            if i == 0:
                y_max = val
            elif val > y_max and df[scorer][bp]['likelihood'][i] > 0.8:
                y_max = val
        
        # this will make an overall comparison among all the bodyparts to know what is the true_max and true_min
        if bp == bodyparts[0]:
            true_min_y = y_min
            true_max_y = y_max
        else:
            if y_min < true_min_y:
                true_min_y = y_min
                
            if y_max > true_max_y:
                true_max_y = y_max
        
        # print('min: ' + str(true_min_y))        
        # print('max: ' + str(true_max_y))  
        
              
        # Getting the max and min point horizontally
        # same thought process as for Y
        x=list(df[scorer][bp]['x'])
        
        for i in range(len(x)):
            valx = x[i]
            if i == 0:
                x_min = valx
            elif val < x_min and df[scorer][bp]['likelihood'][i] > 0.8:
                x_min = valx
            
            if i == 0:
                x_max = valx
            elif valx > x_max and df[scorer][bp]['likelihood'][i] > 0.8:
                x_max = valx
        
        if bp == bodyparts[0]:
            true_min_x = x_min
            true_max_x = x_max
        else:
            if x_min < true_min_x:
                true_min_x = x_min
                
            if x_max > true_max_x:
                true_max_x = x_max

        # print('Xmin: ' + str(true_min_x))        
        # print('Xmax: ' + str(true_max_x))
    
    # Calculate the horizontal distance of the cropping frame to get the center, to then align the vertical dimension
    x_dist = true_max_x - true_min_x
    x_center = true_min_x + (x_dist/2)

    
    # print('x_center: ' + str(x_center))
    
    # Compute the aspect ratio to match horizontally 16:9
    # Add a tiny margin of 20 pixels (10 in the top and 10 on the bottom) - 20 because most of the videos are just in HD
    new_height = int((true_max_y - true_min_y)+20)

    # Normalized coordinates according to the downsampled videos
    if aspect_ratio == "16:9":
        new_width = int((new_height/9)*16)

        # Ordinary/Typical cropping coordinates
        top = (true_min_y-10)
        bottom = (true_max_y+10)
        left = (x_center-(new_width/2))
        right = (x_center+(new_width/2))

        
        # If we crop too much (zooming in too much) we would be interpolating the pixels. We need to check the dimensions of the original video and then make sure that the normalization is not lower
        # than the 512x288 on the original video
        min_height = 288 * (288 / dimensions[1])
        if (bottom-top) < min_height :
            exceed = min_height - (bottom-top)
            top = top - exceed/2
            bottom = bottom + exceed/2
            correct_height = bottom-top
            new_width = int((correct_height/9)*16)
            left = (x_center-(new_width/2))
            right = (x_center+(new_width/2))

        
        incorrect_margin = True # boolean to make sure that the margin is not incorrect and that the cropping zone fits the entire participant
        counter=0
        #All the following conditions either shift the cropping zone in the video or augment it a little bit to include all body parts
        # Extraordinary conditions that might emerge in more weird cases - All the following conditions either shift the cropping zone in the video or augment it a little bit to include all body parts
        while(incorrect_margin):
            
            # copy of the original coordinates to verify if something changed
            top1=top
            bottom1=bottom
            left1=left
            right1=right

            print("0   Top: "+str(top1)+" | Bottom: " + str(bottom1)+ " | Left: " + str(left1) + " | Right: "+ str(right1))
            
            #Condition in case the crop area doesn't cover the extreme point on the left
            if left > true_min_x:
                if left > 0 and true_min_x > 0:
                    x_dist = (x_center - true_min_x)*2 # this will always be bigger than the original
                    left = true_min_x - 10
                    right = x_center + x_dist/2 + 10

                    new_height=((x_dist+20)/16)*9
                    top = x_center-(new_height/2)
                    bottom = x_center+(new_height/2)
            print("1   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #Condition in case the crop area doesn't cover the extreme point on the right
            if right < true_max_x:
                if right < 512 and true_max_x < 512:
                    x_dist = (true_max_x - x_center)*2
                    left = x_center - x_dist/2 - 10
                    right = true_max_x + 10

                    new_height=((x_dist+20)/16)*9
                    top = x_center-(new_height/2)
                    bottom = x_center+(new_height/2)
            print("2   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #condition where the (center - new_width) is below 0
            if left < 0:
                remaining_dist = 0 - left
                left = 0
                right = x_center + remaining_dist + (new_width/2)
            print("3   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))

            #condition where the (center + new_width) is higher than 512
            if right > 512:
                remaining_dist = right - 512 # extra distance that surpasses the limit
                right = 512
                left = x_center - ((new_width/2)+remaining_dist) #add it ot the ohter side
            print("4   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            # Condition where top crop coord passes the top limit (y=0)
            if top < 0 :
                extra = 0 - top
                top = 0
                bottom = bottom + extra
            print("5   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            # Condition where bottom crop coord passes the bottom limit (y=288)
            if bottom > 288 :
                extra = bottom-288
                top = top - extra
                bottom = 288
            print("6   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #the loop doesn't find an appropriate crooping condition, so it does not
            if counter > 10:
                top = 0
                bottom = 288
                left = 0
                right = 512
                print('Not croppped!')
            print("7   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))

            # this if guarantes the coordinates are correct and that all conditions in the loop is ok
            if top >= 0 and bottom <= 288 and left >= 0 and right <= 512 and top == top1 and bottom == bottom1 and left == left1 and right == right1:
                incorrect_margin = False

            counter = counter +1


        coord = [top/288, bottom/288, left/512, right/512]
        return coord
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Exactly the same thought process than in 16:9
    elif aspect_ratio == "4:3": #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        new_width = int((new_height/3)*4)

        # Ordinary/Typical cropping coordinates
        top = (true_min_y-10)
        bottom = (true_max_y+10)
        left = (x_center-(new_width/2))
        right = (x_center+(new_width/2))

        incorrect_margin = True

        # If we crop too much, we would be interpolating the pixels. We need to check the dimensions of the original video and then make sure that the normalization is not lower
        # than the 540*404 no video original
        min_height = 404 * (404 / dimensions[1])
        if (bottom-top) < min_height :
            exceed = min_height - (bottom-top)
            top = top - exceed/2
            bottom = bottom + exceed/2
            correct_height = bottom-top
            new_width = int((correct_height/3)*4)
            left = (x_center-(new_width/2))
            right = (x_center+(new_width/2))
        counter=0
        # Extraordinary conditions that might emerge in more weird cases
        while(incorrect_margin):
            top1=top
            bottom1=bottom
            left1=left
            right1=right
            print("0   Top: "+str(top1)+" | Bottom: " + str(bottom1)+ " | Left: " + str(left1) + " | Right: "+ str(right1))
            
            #Condition in case the crop doesn't cover the extreme point on the left
            if left > true_min_x:
                if left > 0 and true_min_x > 0:
                    x_dist = (x_center - true_min_x)*2 # this will always be bigger than the original
                    left = true_min_x - 10
                    right = x_center + x_dist/2 + 10

                    new_height=((x_dist+20)/4)*3
                    top = x_center-(new_height/2)
                    bottom = x_center+(new_height/2)
            print("1   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #Condition in case the crop doesn't cover the extreme point on the right
            if right < true_max_x:
                if right < 540 and true_max_x < 540:
                    x_dist = (true_max_x - x_center)*2
                    left = x_center - x_dist/2 - 10
                    right = true_max_x + 10

                    new_height=((x_dist+20)/4)*3
                    top = x_center-(new_height/2)
                    bottom = x_center+(new_height/2)
            print("2   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #condition where the (center - new_width) is below 0
            if left < 0:
                left = 0
                remaining_dist = new_width - x_center
                right = x_center + remaining_dist
            print("3   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            

            #condition where the (center + new_width) is higher than 540
            if right > 540:
                right = 540
                remaining_dist = 540-x_center
                left = x_center - ((new_width/2)+remaining_dist)
            print("4   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            

            # Condition where top crop coord passes the top limit (y=0)
            if top < 0 :
                if counter < 6:
                    top = 0
                    new_width = int((bottom/3)*4)
                    left = (x_center-(new_width/2))
                    right = (x_center+(new_width/2))
                elif counter >= 6:
                    extra = 0 - top
                    top = 0
                    bottom = bottom + extra
            print("5   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            # Condition where bottom crop coord passes the bottom limit (y=404)
            if bottom > 404 :
                
                if counter < 6 :
                    bottom = 404
                    new_width = int(((404-top)/3)*4)
                    left = (x_center-(new_width/2))
                    right = (x_center+(new_width/2))
                if counter >= 6 :
                    extra = bottom-404
                    top = top + extra
                    bottom = 404
            print("6   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #the loop doesn't find an appropriate crooping condition, so it does not crop
            if counter > 10:
                top = 0
                bottom = 404
                left = 0
                right = 540

            print("7   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            # this if guarantes the coordinates are correct and that all conditions in the loop are ok
            if top >= 0 and bottom <= 404 and left >= 0 and right <= 540 and top == top1 and bottom == bottom1 and left == left1 and right == right1:
                incorrect_margin = False

            counter = counter +1


        coord = [top/404, bottom/404, left/540, right/540]
        return coord

#============================================================= Iteration between the videos =================================================================

#Path to the folder with the results from the first analysis - the simply downsampled videos
res_folder_path = r'D:\_users\Duarte_Projects\LL_Correct_Crop-Duarte-2022-05-16\videos\LD_ORIGINAL_Results'

database_folder = r'\\svfas5.epfl.ch\blanke-lab\Users\Duarte\Data\Old_recordings\Video'# Original database with the Full-HD videos 

text_file_path = res_folder_path + r'\cropping_coords.txt' # Name of the text file that will save the data -  It cannot write in the Blanke Lab

temp = ''
for root, dirs, files in os.walk(res_folder_path):
    for filename in files:   
        
        # we don't want to analyse .txt files (this should be the only file type on this folder)
        if 'txt' in filename:
            break

        # we don't want anything inside the plot poses folder
        if 'plot-poses' not in root:
            videoname = os.path.basename(root)
            
            if videoname[-1] == 'd':
                videoname = videoname[:-1]

        # print('videoname: ' + videoname)
        if videoname != temp:
            for root2, dirs, files in os.walk(root):
                for name in files:
                    if '.h5' in name:
                        if 'skeleton' not in name and 'standardized' not in name:
                            h5_path = os.path.join(root,name) # directory + '/' + name
                            print('Start to downsample: ' + videoname)

                            # In here i have to put the search funtion on the original database to get the videopath
                            videopath = search_db(database_folder,videoname)
                            if videopath == None:
                                print('Not in the database!')
                                break
                            else:
                                print('Original video found in: '+ videopath)

                            #Use movie py to get the video size and check the aspect ratio
                            clip = VideoFileClip(videopath)
                            dimensions = clip.size
                            clip.close()

                            print('Video dimensions collected')
                            dim_ratio = dimensions[0]/dimensions[1]
                            
                            if round(dim_ratio,2) == 1.78 :
                                #The aspect ratio is 16:9
                                aspect_ratio = "16:9"
                            elif round(dim_ratio,2) == 1.33 :
                                #the aspect ratio is 4:3
                                aspect_ratio = "4:3"
                            else:
                                print('The ' + videoname + ' is not in a recognized aspect ratio.')
                                aspect_ratio = ""


                            # Time to crop the original video based on the coordinates from the labeled video - the result is normalized
                            coord = crop_coord(h5_path, aspect_ratio,dimensions)
                            print('Cropping coordinates extracted')

                            #piece of code to test that the coordinates are cropping correctly

                            # clip = crop(clip, x1=int(coord[2]*dimensions[0]), y1=int(coord[0]*dimensions[1]), x2=int(coord[3]*dimensions[0]), y2=int(coord[1]*dimensions[1]))
                            # clip.preview(fps = 100)
                            # clip.ipython_display(t = 2)
                            # clip.close()

                            save_txt = open(text_file_path,"a") # "a" both appends and writes and creates file
                            info = videopath + ' | ' + str(coord) +' | '+ videoname + ' | ' + aspect_ratio + ' | '+ str(dimensions) + '\n'
                            save_txt.write(info)
                            save_txt.close()
                            print('Info saved in the .txt file.')

        temp = videoname # this prevents from writing multiple entries in the .txt file about the same video



