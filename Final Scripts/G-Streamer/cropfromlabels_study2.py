'''
Project: Computational models for robot-induced hallucinations in Parkinson's Disease
Laboratory of Cognitive Neuroscience - LNCO
Author: Duarte Rodrigues
Script: This document is similar to the cropfrompredictions_study1.py. However, this one is adapted for study2.
The main difference is that in here, the cropping coordinates are based on the hand labels, instead of being based
on an initial set of predictions. This works properly because on study2 the recording is much more controlled and the cameras
remain still while the participant is performing the task.

The point is to look to the labels points and determine what was the most extreme point left and right.

By respecting the aspect ratio of the video it is possible to isolate a region of interest, where the participant is, 
and consequently the cropping coordinate to achieve that area.

A series of corrections are applied, because cropping based on the aspect ratio can leave body parts out of frame.
The optimal set of cropping coordinates are then standardized for them to be 0 to 1.

In the end a text file is created, where each line has the information regarding a video for it to be cropped.
    videopath | optimal crop coordinates | videoname | aspect ratio | original dimensions

''' 
import os
from moviepy.editor import *
import pandas as pd
import math

def search_db(path_db, videoname):
    ''' This function takes the path of the original database of videos (without being downcropped) and searches it to 
    find where the video with the same name is and returns the complete path of it.'''
    
    for root, dirs, files in os.walk(path_db):
        
        for filnam in files:
            
            if videoname in filnam:
                videopath = os.path.join(root,filnam)
                
                return videopath


def crop_coord(h5_path, aspect_ratio,dimensions):
    """
    Calculates the coordinates to crop a video to a specified aspect ratio and with a certain margin.
    It checks if the resulting cropping region covers the whole bodyparts.
    Made for the study2 in mind, when I tried to run it as a multi animal DLC model. It uses the groundtruth label data file (because all the videos were labeled and the cameras do not move during the experiment)
    
    Parameters:
    h5_path (str): path to the h5 file
    aspect_ratio (str): aspect ratio for cropping, the options are "16:9" or "4:3".
    dimensions (tuple): dimension of the video to be cropped in pixels, this is (width, height)
    
    Returns:
    coord (dict): A dictionary with the cropping coordinates in pixels, the keys are "top", "bottom", "left", "right"
    """
    videoname = os.path.basename(os.path.dirname(h5_path))
    # nanp=r'PD_7-1_R_sync_right_angle' #participant with just nan

    # since it is the label .h5 data file it is easier to load the data like this
    df = pd.read_hdf(h5_path)
    mi = pd.MultiIndex.from_frame(df)
    scorer = mi.get_level_values(0).name[0]
    individuals = mi.get_level_values(0).name[1]
    num_col = len(df.columns)
    
    # needs to be set like this to read the bodyparts based on how the .h5 is configured
    bodyparts = []
    for i in range(0,num_col,2): # 3 - results .h5 file
        bodyparts.append(mi.get_level_values(i).name[2])
    # print(bodyparts) # same as the excel
    
    # it's going through the all the bodyparts to check what was the most extreme point.
    for bp in bodyparts:

        # Getting the max and min points vertically
        y=list(df[scorer][individuals][bp]['y'])

        # I don't need to verify the likelihood because these are hand labels. the researcher is certain that he puts them in the right place.
        if math.isnan(y_max):
            y_max = 0
        
        y_min = min(y)
        if math.isnan(y_min):
            y_min = dimensions[1]
            
        # this will make an overall comparison among all the bodyparts to know what is the true_max and true_min
        if bp == bodyparts[0]:
            true_min_y = y_min
            true_max_y = y_max
        else:
            if y_min < true_min_y:
                true_min_y = y_min
                
            if y_max > true_max_y:
                true_max_y = y_max
        
        # print('min: ' + str(true_min_y))        
        # print('max: ' + str(true_max_y))        
        
        # Getting the max and min point horizontally
        # same thought process as for Y
        x=list(df[scorer][individuals][bp]['x'])
        
        x_max = max(x)
        if math.isnan(x_max):
            x_max = 0

        x_min = min(x)
        if math.isnan(x_min):
            x_min = dimensions[0]
        
        if bp == bodyparts[0]:
            true_min_x = x_min
            true_max_x = x_max
        else:
            if x_min < true_min_x:
                true_min_x = x_min
                
            if x_max > true_max_x:
                true_max_x = x_max

    # Calculate the horizontal distance of the cropping frame to get the center, to then align the vertical dimension
    x_dist = true_max_x - true_min_x
    x_center = true_min_x + (x_dist/2)
    
    # print('x_center: ' + str(x_center))
    # Compute the aspect ratio to match horizontally 16:9
    # Add a tiny margin of 60 pixels (30 in the top and 30 on the bottom) - 60 because most of the videos are in FullHD, so more pixels are needed to achieve the same margin
    new_height = int((true_max_y - true_min_y)+60)

    
    if aspect_ratio == "16:9":
        new_width = int((new_height/9)*16)

        # Ordinary/Typical cropping coordinates
        top = (true_min_y-30)
        bottom = (true_max_y+30)
        left = (x_center-(new_width/2))
        right = (x_center+(new_width/2))

        

        # If we crop too much (zooming in too much), we would be interpolating the pixels. We need to check the dimensions of the original video and then make sure that the normalization is not lower
        # than the 512x288 in the original video 
        min_height = 288
        if (bottom-top) < min_height :
            exceed = min_height - (bottom-top)
            top = top - exceed/2
            bottom = bottom + exceed/2
            correct_height = bottom-top
            new_width = int((correct_height/9)*16)
            left = (x_center-(new_width/2))
            right = (x_center+(new_width/2))

        incorrect_margin = True # boolean to make sure that the margin is not incorrect and that the cropping zone fits the entire participant
        #All the following conditions either shift the cropping zone in the video or augment it a little bit to include all body parts
        counter=0
        # Extraordinary conditions that might emerge in more weird cases
        while(incorrect_margin):
            
            # copy of the original coordinates to verify if something changed
            top1=top
            bottom1=bottom
            left1=left
            right1=right
            
            print("0   Top: "+str(top1)+" | Bottom: " + str(bottom1)+ " | Left: " + str(left1) + " | Right: "+ str(right1))
            
            #Condition in case the crop doesn't cover the extreme point on the left
            if left > true_min_x:
                if left > 0 and true_min_x > 0:
                    x_dist = (x_center - true_min_x)*2 # this will always be bigger than the original
                    left = true_min_x - 30
                    right = x_center + x_dist/2 + 30

                    new_height=((x_dist+100)/16)*9
                    top = x_center-(new_height/2)
                    bottom = x_center+(new_height/2)
            print("1   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #Condition in case the crop doesn't cover the extreme point on the right
            if right < true_max_x:
                if right < dimensions[0] and true_max_x < dimensions[0]:
                    x_dist = (true_max_x - x_center)*2
                    left = x_center - x_dist/2 - 30
                    right = true_max_x + 30

                    new_height=((x_dist+60)/16)*9
                    top = x_center-(new_height/2)
                    bottom = x_center+(new_height/2)
            print("2   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #condition where the (center - new_width) is below 0
            if left < 0:
                remaining_dist = 0 - left
                left = 0
                right = right + remaining_dist
            print("3   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))

            #condition where the (center + new_width) is higher than width
            if right > dimensions[0]:
                remaining_dist = right - dimensions[0] # extra distance that surpasses the limit
                right = dimensions[0]
                left = left - remaining_dist
            print("4   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            # Condition where top crop coord passes the top limit (y=0)
            if top < 0 :
                extra = 0 - top
                top = 0
                bottom = bottom + extra
            print("5   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            # Condition where bottom crop coord passes the bottom limit (y=288)
            if bottom > dimensions[1] :
                extra = bottom-dimensions[1]
                top = top - extra
                bottom = dimensions[1]
            print("6   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
            #the loop doesn't find an appropriate crooping condition, so it does not
            if counter > 10:
                top = 0
                bottom = dimensions[1]
                left = 0
                right = dimensions[0]
                print('Not croppped!')
            print("7   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))

            # this if guarantes the coordinates are correct and that all conditions in the loop is ok
            if top >= 0 and bottom <= dimensions[1] and left >= 0 and right <= dimensions[0] and top == top1 and bottom == bottom1 and left == left1 and right == right1:
                incorrect_margin = False

            counter = counter +1

        ratio = str((right-left)/(bottom-top)) #aspect_ratio of the cropping region
        coord = [top/dimensions[1], bottom/dimensions[1], left/dimensions[0], right/dimensions[0]]
        
        return coord, ratio
    
    """
    On study2 there is no videos recorded on 4:3 aspectio ratio, because the protocol was standardized for the cameras to be always the same on the all the participants
    # #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ other aspectio ratio - Never happens in study 2+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # elif aspect_ratio == "4:3": #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #     new_width = int((new_height/3)*4)

    #     # Ordinary/Typical cropping coordinates
    #     top = (true_min_y-10)
    #     bottom = (true_max_y+10)
    #     left = (x_center-(new_width/2))
    #     right = (x_center+(new_width/2))

    #     incorrect_margin = True

    #     # If we crop too much, we would be interpolating the pixels. We need to check the dimensions of the original video and then make sure that the normalization is not lower
    #     # than the 540*404 no video original
    #     min_height = 404 * (404 / dimensions[1])
    #     if (bottom-top) < min_height :
    #         exceed = min_height - (bottom-top)
    #         top = top - exceed/2
    #         bottom = bottom + exceed/2
    #         correct_height = bottom-top
    #         new_width = int((correct_height/3)*4)
    #         left = (x_center-(new_width/2))
    #         right = (x_center+(new_width/2))
    #     counter=0
    #     # Extraordinary conditions that might emerge in more weird cases
    #     while(incorrect_margin):
    #         top1=top
    #         bottom1=bottom
    #         left1=left
    #         right1=right
    #         print("0   Top: "+str(top1)+" | Bottom: " + str(bottom1)+ " | Left: " + str(left1) + " | Right: "+ str(right1))
            
    #         #Condition in case the crop doesn't cover the extreme point on the left
    #         if left > true_min_x:
    #             if left > 0 and true_min_x > 0:
    #                 x_dist = (x_center - true_min_x)*2 # this will always be bigger than the original
    #                 left = true_min_x - 10
    #                 right = x_center + x_dist/2 + 10

    #                 new_height=((x_dist+20)/4)*3
    #                 top = x_center-(new_height/2)
    #                 bottom = x_center+(new_height/2)
    #         print("1   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
    #         #Condition in case the crop doesn't cover the extreme point on the right
    #         if right < true_max_x:
    #             if right < 540 and true_max_x < 540:
    #                 x_dist = (true_max_x - x_center)*2
    #                 left = x_center - x_dist/2 - 10
    #                 right = true_max_x + 10

    #                 new_height=((x_dist+20)/4)*3
    #                 top = x_center-(new_height/2)
    #                 bottom = x_center+(new_height/2)
    #         print("2   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
    #         #condition where the (center - new_width) is below 0
    #         if left < 0:
    #             left = 0
    #             remaining_dist = new_width - x_center
    #             right = x_center + remaining_dist
    #         print("3   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            

    #         #condition where the (center + new_width) is higher than 540
    #         if right > 540:
    #             right = 540
    #             remaining_dist = 540-x_center
    #             left = x_center - ((new_width/2)+remaining_dist)
    #         print("4   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            

    #         # Condition where top crop coord passes the top limit (y=0)
    #         if top < 0 :
    #             if counter < 6:
    #                 top = 0
    #                 new_width = int((bottom/3)*4)
    #                 left = (x_center-(new_width/2))
    #                 right = (x_center+(new_width/2))
    #             elif counter >= 6:
    #                 extra = 0 - top
    #                 top = 0
    #                 bottom = bottom + extra
    #         print("5   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
    #         # Condition where bottom crop coord passes the bottom limit (y=404)
    #         if bottom > 404 :
                
    #             if counter < 6 :
    #                 bottom = 404
    #                 new_width = int(((404-top)/3)*4)
    #                 left = (x_center-(new_width/2))
    #                 right = (x_center+(new_width/2))
    #             if counter >= 6 :
    #                 extra = bottom-404
    #                 top = top + extra
    #                 bottom = 404
    #         print("6   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
            
    #         #the loop doesn't find an appropriate crooping condition, so it does not crop
    #         if counter > 10:
    #             top = 0
    #             bottom = 404
    #             left = 0
    #             right = 540

    #         print("7   Top: "+str(top)+" | Bottom: " + str(bottom)+ " | Left: " + str(left) + " | Right: "+ str(right))
    #         # this if guarantes the coordinates are correct and that all conditions in the loop are ok
    #         if top >= 0 and bottom <= 404 and left >= 0 and right <= 540 and top == top1 and bottom == bottom1 and left == left1 and right == right1:
    #             incorrect_margin = False

    #         counter = counter +1


    #     coord = [top/404, bottom/404, left/540, right/540]
    #     return coord
""" 
#============================================================= Iteration between the videos =================================================================

#Path to the folder with the labels
label_folder_path = r'D:\_users\Duarte_Projects\ma_new_DB_SingleView-Duarte-2022-07-12\labeled-data'

database_folder = r"D:\_users\Duarte_Projects\new_DB_Conditions" # Original database with the Full-HD videos 

text_file_path = label_folder_path + r'\cropping_coords11.txt' # Name of the text file that will save the data - It cannot write in the Blanke Lab

temp = ''
for root, dirs, files in os.walk(label_folder_path):
    for filename in files:
           
        # we don't want to analyse .txt files (this should be the only file type on this folder)
        if 'txt' in filename:
            break

        # we don't want anything inside the plot poses folder - this shouldnt be a problem here since we are not going through the results directory
        if 'plot-poses' not in root:
            videoname = os.path.basename(root)
            
            if videoname[-1] == 'd':
                videoname = videoname[:-1]

        # print('videoname: ' + videoname)
        if videoname != temp:
            for root2, dirs, files in os.walk(root):
                for name in files:
                    if '.h5' in name:
                    
                        h5_path = os.path.join(root,name) # directory + '/' + name
                        print('Start to extract crop coord: ' + videoname)

                        # In here i have to put the search funtion on the db to get the videopath
                        videopath = search_db(database_folder,videoname) # Verify if the correct angle of filming is being selected
                        if videopath == None:
                            print('Not in the database!')
                            break
                        else:
                            print('Original video found in: '+ videopath)

                        #Use movie py to get the video size and check the aspect ratio
                        clip = VideoFileClip(videopath)
                        dimensions = clip.size
                        clip.close()

                        print('Video dimensions collected')
                        dim_ratio = dimensions[0]/dimensions[1]
                        
                        if round(dim_ratio,2) == 1.78 :
                            #The aspect ratio is 16:9 -> Case of the database on study2
                            aspect_ratio = "16:9"
                        elif round(dim_ratio,2) == 1.33 :
                            #the aspect ratio is 4:3 (I LEFT IT JUST IN CASE)
                            aspect_ratio = "4:3"
                        else:
                            print('The ' + videoname + ' is not in a recognized aspect ratio.')
                            aspect_ratio = ""

                        ################## THIS WAS WRITTEN TO EXTRACT LABELS GIVEN ON VIDEOS full HD AND NOT DOWNSAMPLED!!!!!!########################################
                        ################## THE ASPECT RATIO IS 16:9 EVERYTIME!!!! ################################################################################ (just a gentle reminder)
                        # Time to crop the original video based on the coordinates from the labeled video - normalized
                        coord, ratio = crop_coord(h5_path, aspect_ratio,dimensions)
                        print('Cropping coordinates extracted')

                        #piece of code to test that the coordinates are cropping correctly

                        # clip = crop(clip, x1=int(coord[2]*dimensions[0]), y1=int(coord[0]*dimensions[1]), x2=int(coord[3]*dimensions[0]), y2=int(coord[1]*dimensions[1]))
                        # clip.preview(fps = 100)
                        # clip.ipython_display(t = 2)

                        # clip.close()

                        save_txt = open(text_file_path,"a") # "a" both appends and writes and creates file
                        info = videopath + ' | ' + str(coord) +' | '+ videoname + ' | ' + aspect_ratio + ' | '+ str(dimensions) + ' | ' + ratio + '\n'
                        save_txt.write(info)
                        save_txt.close()
                        print('Info saved in the .txt file.')

        temp = videoname # this prevents from writing multiple entries in the .txt file about the same video



