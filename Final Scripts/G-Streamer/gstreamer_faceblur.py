'''
Project: Computational models for robot-induced hallucinations in Parkinson's Disease
Laboratory of Cognitive Neuroscience - LNCO
Author: Duarte Rodrigues
Script: This document was a first try to do the blured face of participants based on the automatic functions 
of g_streamer (that is based on the computer vision algorithms on cv2, if I am not mistaken).

This didn't work very well, because most of the videos have a lateralized view and the faces could not be automatically recognized.
It only works with faces that are directly looking in front (to the camera) and in HD quality.
To blur the faces, the faceblur.py was developped based no the chin predictions given by DLC.

NOTE: This file can only be compiled directly on the command line of MINGW/MSYS as shown in the G_streamer examples below.
''' 
import os
import gi
import sys
import traceback
import shutil

from moviepy.editor import *
from moviepy.video.fx.all import crop

gi.require_version("Gst", "1.0")
gi.require_version("GstApp", "1.0")

from gi.repository import Gst, GstApp, GLib

#=====================================================Gstreamer Init=========================================================================

_ = GstApp
Gst.init(sys.argv)

#=========================================================Gstreamer examples to base myself=====================================================================

# FOLLOW THIS STEPS TO RUN THIS SCRIPT
#cd d:/_users/Duarte_Projects/g_streamer_src
#python3 gstreamer_faceblur.py

#pay attention that in the path the orientation of the slash should be this /
# pt = "D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4"

#OTHER COMMAND EXAMPLES AND WHAT THEY DO
# To get info
# gst-inspect-1.0.exe
# gst-inspect-1.0.exe nvh264enc

#To save 
# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! x264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov

# To display
#gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! videoconvert ! autovideosink


# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! videobox top=100 left=300 right=300 bottom=100 ! x264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov

# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! videobox top=100 left=300 right=300 bottom=100 ! videoscale ! video/x-raw, width=320, height=240 ! nvh264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov
#============================================================= Proper code =================================================================

def on_message(bus: Gst.Bus, message: Gst.Message, loop: GLib.MainLoop):
    mtype = message.type
    """ Necessary message and flag communication of G_Streamer to work on python
        Gstreamer Message Types and how to parse
        https://lazka.github.io/pgi-docs/Gst-1.0/flags.html#Gst.MessageType
    """
    if mtype == Gst.MessageType.EOS:
        print("End of stream")
        loop.quit()

    elif mtype == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(err, debug)
        loop.quit()
    elif mtype == Gst.MessageType.WARNING:
        err, debug = message.parse_warning()
        print(err, debug)

    return True

def g_stream_faceblur(videopath, dimensions,rate):
    #============================================================= Gstreamer application =================================================================
    
    # videopath of the video intended to blur
    save = os.path.dirname(videopath)

    #Necessary correction for g_streamer to work
    gpath = videopath.replace('\\','/')
    gsave = save.replace('\\','/')

    videoname = os.path.basename(videopath)
    print('G-STREAMER Start to downsample and scale the ' + videoname[:-4] + ' !')

    #They need this file! I guess it is the face detection model
    profile_path = r"C:\msys64\mingw64\share\opencv4\haarcascades\haarcascade_frontalface_default.xml"
    gprofile = profile_path.replace('\\','/')
    
    # This is the actual g_streamer command pipeline to decode and parse the video, to then apply the changes and them rebuilding a new video with those changes
    pipeline = Gst.parse_launch("uridecodebin uri=file:///" + gpath + " ! queue ! videoconvert ! faceblur profile=" + gprofile + " ! videoconvert ! nvh264enc ! h264parse ! qtmux ! filesink location=" + gsave + '/' + videoname[:-4] + "_FaceBlured.mp4") # width=" + str(dimensions[0]) +", height=" + str(dimensions[1]) +", framerate=" + str(rate) + "
        
    # From here on out the rest of the function are the comands needed for python to integrate successfully with g_streamer
    
    bus = pipeline.get_bus()

    # allow bus to emit messages to main thread
    bus.add_signal_watch()

    # Start pipeline
    pipeline.set_state(Gst.State.PLAYING)

    # Init GObject loop to handle Gstreamer Bus Events
    loop = GLib.MainLoop()

    # Add handler to specific signal
    bus.connect("message", on_message, loop)

    try:
        loop.run()
    except Exception:
        traceback.print_exc()
        loop.quit()

    # Stop Pipeline
    pipeline.set_state(Gst.State.NULL)
    del pipeline


from tkinter import Tk  
from tkinter.filedialog import askopenfilename
Tk().withdraw()

# NOTE: When you run this cell, a new window opens to select the database folder with all the results. The new window
# usually opens behind the code editor software.
# select the video to blur, preferably in HD
videopath = askopenfilename(title='Choose video to blur', initialdir=r'\\sv1arch\lnco-archives\usr\Duarte')

clip = VideoFileClip(videopath)
dimensions = clip.size
rate = clip.fps
clip.close()

# Calling the function to apply the face blur
g_stream_faceblur(videopath, dimensions,rate)