'''
Project: Computational models for robot-induced hallucinations in Parkinson's Disease
Laboratory of Cognitive Neuroscience - LNCO
Author: Duarte Rodrigues
Script: Before running this file make sure that you ran one of the cropping scripts depending on the database to originate the .txt file.
This documents uses the .txt file with the information regarding each video and its cropping coordinates.
Based on that, it uses G_streamer to do the cropping and dwonsampling in a more efficient way. resulting in a DownCrop video.

NOTE: This file can only be compiled directly on the command line of MINGW/MSYS as shown in the G_streamer examples below.'''
import os
import gi
import sys
import traceback
import shutil
gi.require_version("Gst", "1.0")
gi.require_version("GstApp", "1.0")

from gi.repository import Gst, GstApp, GLib

#=====================================================Gstreamer Init=========================================================================

_ = GstApp
Gst.init(sys.argv)

#=========================================================Gstreamer examples to base myself=====================================================================

# FOLLOW THIS STEPS TO RUN THIS SCRIPT
#cd d:/_users/Duarte_Projects/g_streamer_src
#python3 gstreamer_ROIcrop.py

# Pay attention that in the path the orientation of the slash should be this /
# pt = "D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4"

# To get info
# gst-inspect-1.0.exe
# gst-inspect-1.0.exe nvh264enc

#To save 
# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! x264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov

# To display
#gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! videoconvert ! autovideosink


# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! videobox top=100 left=300 right=300 bottom=100 ! x264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov

# gst-launch-1.0.exe uridecodebin uri=file:///D:/_users/Duarte_Projects/g_streamer_src/HC_7_try_out.MP4 ! queue ! videoconvert ! videobox top=100 left=300 right=300 bottom=100 ! videoscale ! video/x-raw, width=320, height=240 ! nvh264enc ! h264parse ! qtmux ! filesink location=./rescaled.mov


#============================================================= Proper code =================================================================

def on_message(bus: Gst.Bus, message: Gst.Message, loop: GLib.MainLoop):
    mtype = message.type
    """ Necessary message and flag communication of G_Streamer to work on python.
        Gstreamer Message Types and how to parse
        https://lazka.github.io/pgi-docs/Gst-1.0/flags.html#Gst.MessageType
    """
    if mtype == Gst.MessageType.EOS:
        print("End of stream")
        loop.quit()

    elif mtype == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(err, debug)
        loop.quit()
    elif mtype == Gst.MessageType.WARNING:
        err, debug = message.parse_warning()
        print(err, debug)

    return True

def g_stream_crop_scale(videopath,coord,videoname, aspect_ratio, dimensions):
    #============================================================= Gstreamer application =================================================================
    # Path for the video that is going to be cropped
    save = os.path.dirname(videopath)

    #Necessary correction for g_streamer to work
    gpath = videopath.replace('\\','/')
    gsave = save.replace('\\','/')
    print('G-STREAMER Start to downsample and scale the ' + videoname + ' !')
    
    # This is the actual g_streamer command pipeline to decode and parse the video, to then apply the changes and them rebuilding a new video with those changes
    # It needs to be separated into different aspect_ratios because the final video is saved with specific fixed values for width and height, for it to be a good trade-off between video clarity and low computation times on DLC.
    # The rest of the command line is the same
    if aspect_ratio == "16:9":
        pipeline = Gst.parse_launch("uridecodebin uri=file:///" + gpath + " ! queue ! videoconvert ! videobox top=" + str(int(coord[0]*dimensions[1])) + " left=" + str(int(coord[2]*dimensions[0])) + " right=" + str(int(dimensions[0]-(coord[3]*dimensions[0]))) + " bottom=" + str(int(dimensions[1]-(coord[1]*dimensions[1]))) + " ! videoscale ! video/x-raw, width=512, height=288 ! nvh264enc ! h264parse ! qtmux ! filesink location=" + gsave + '/' + videoname + "_DownCrop.mp4") #
    elif aspect_ratio == "4:3":
        pipeline = Gst.parse_launch("uridecodebin uri=file:///" + gpath + " ! queue ! videoconvert ! videobox top=" + str(int(coord[0]*dimensions[1])) + " left=" + str(int(coord[2]*dimensions[0])) + " right=" + str(int(dimensions[0]-(coord[3]*dimensions[0]))) + " bottom=" + str(int(dimensions[1]-(coord[1]*dimensions[1]))) + " ! videoscale ! video/x-raw, width=540, height=404 ! nvh264enc ! h264parse ! qtmux ! filesink location=" + gsave + '/' + videoname + "_DownCrop.mp4")

    # From here on out the rest of the function are the comands needed for python to integrate successfully with g_streamer
    
    bus = pipeline.get_bus()

    # allow bus to emit messages to main thread
    bus.add_signal_watch()

    # Start pipeline
    pipeline.set_state(Gst.State.PLAYING)

    # Init GObject loop to handle Gstreamer Bus Events
    loop = GLib.MainLoop()

    # Add handler to specific signal
    bus.connect("message", on_message, loop)

    try:
        loop.run()
    except Exception:
        traceback.print_exc()
        loop.quit()

    # Stop Pipeline
    pipeline.set_state(Gst.State.NULL)
    del pipeline

#Both this paths in to be on a local drive (C:\ or D:\)
text_file_path = r"D:\_users\Duarte_Projects\ma_new_DB_SingleView-Duarte-2022-07-12\labeled-data\cropping_coords11_5.txt" # Path of the text file with the information for the cropping
change_path = r'D:\_users\Duarte_Projects' # the g_streamer has problems reading and writing on non-local drives (such as the Blanke-Lab or the archives)

# readin the text file
with open(text_file_path, "r") as a_file:
    for line in a_file:
        
        #Method for reading and storing the info separated by | in a list
        info = line.strip()
        res = [i for i in range(len(info)) if info.startswith("|", i)]

        # Need to copy all videos of interest to folder in D: drive
        videopath = info[0:(res[0]-1)]

        # UNCOMMENT IN CASE IT IS NOT IN BLANKE DRIVE
        # loc = videopath.find('Video')
        # videopath = videopath[:loc+6] + 'original\\' + videopath[loc+6:]
        # videopath = change_path + videopath[loc+5:] # this will 

        # cropping coordinates
        str_coord = info[(res[0]+2):(res[1]-1)]
        coord = list(map(float, str_coord[1:-1].split(', ')))

        videoname = info[(res[1]+2):(res[2]-1)]

        aspect_ratio = info[(res[2]+2):(res[3]-1)]

        # dimensions of the orginal video
        str_dimensions = info[(res[3]+2):(res[4]-1)]
        dimensions = list(map(int, str_dimensions[1:-1].split(', ')))
        
        crop_ratio = info[(res[4]+2):]
        if crop_ratio[:4]!='1.77':
            print('The cropping of '+videoname+' is wrong.')

        # apply the cropping using g_streamer - reminder: the videopath needs to be on a local drive
        g_stream_crop_scale(videopath,coord,videoname,aspect_ratio,dimensions)
    
# Needs to be a local drive
directory = r"D:\_users\Duarte_Projects\new_DB_Conditions"
new_dir= r"D:\_users\Duarte_Projects\new_DB_Conditions_Downcrop"
ind=len(directory)

# Copies the DownCrop videos from the directory to the new_dir, with the same folder organization
for root, dirs, files in os.walk(directory):
    for filename in files:
        if 'DownCrop' in filename:
            # os.remove(os.path.join(root, filename))
            #downsampled path
            videoname=os.path.join(root, filename)

            #getting the tree folder arrangement
            subfolders=videoname[ind:]
            new_folder=os.path.dirname(new_dir + subfolders)

            # In case it doesn't exists makes a new one
            print((new_folder))
            if not os.path.isdir(new_folder):
                os.makedirs(new_folder)

            #moving there the DownCrop video
            shutil.move(videoname,new_folder)

