'''
Project: Computational models for robot-induced hallucinations in Parkinson’s Disease
Laboratory of Cognitive Neuroscience - LNCO
Author: Duarte Rodrigues
Script: This document takes the stereocospic video recording (1 hour) and separates it into 2 videos.
It saves them initially in the same folder and then moves them to another folder to store all the videos 
properly (in this case it is going to D:\_users\Duarte_Projects)
'''

# main library import
from tkinter import Tk  
from tkinter.filedialog import askdirectory
import os
from moviepy.editor import *
from moviepy.video.fx.all import crop
import shutil

# Choose the folder with the participants that were filmed with the stereocospic view
# NOTE:  When you run this cell, a new window opens to select the database folder with all the results. The new window
# usually opens behind the code editor software.
Tk().withdraw()
participant_folder = askdirectory(title='Choose folder with of participants filmed in stereo', initialdir=r'\\sv1arch\lnco-archives\usr\Duarte') # show an "Open" dialog box and return the path to the selected file

for root, dirs, files in os.walk(participant_folder):
    if 'Robot behavioural task' in root: # it enters the participant directory and looks for the 'Robot behavioural task' folder
        splitted = False
        for vids in os.listdir(root):
            if 'Right_Angle' in vids: # if there it finds things that are already splitted then it can move forward
                splitted = True
        
        if splitted == False:
            
            stereo_video = root + '\\' + os.listdir(root)[0] # there is only 1 video per participant since it was not divided into task conditions
            print(stereo_video)
            task_folder = root

            clip = VideoFileClip(stereo_video)
            dimensions = clip.size #dim[0]->width

            left_clip = crop(clip,x1=0, y1=0, y2= dimensions[1], x2 = dimensions[0]/2) # from the top left corner to the middle
            rescaled_left_clip  = left_clip.resize( (512,288) )
            videoname_left = os.path.basename(stereo_video)[:-4] + "_Right_Angle.mp4" # the left video, corresponds to the right side of the participant body
            

            right_clip = crop(clip,x1=dimensions[0]/2, y1=0, y2= dimensions[1], x2 = dimensions[0]) # from the middle to the bottom right corner
            rescaled_right_clip  = right_clip.resize( (512,288) )
            videoname_right = os.path.basename(stereo_video)[:-4] + "_Left_Angle.mp4" # the right video, corresponds to the left side of the participant body

            # saving the clips
            rescaled_left_clip.write_videofile(videoname_left,audio=False, codec = 'h264_nvenc', threads = 2)
            rescaled_right_clip.write_videofile(videoname_right,audio=False, codec = 'h264_nvenc', threads = 2)

            clip.close()
            left_clip.close()
            rescaled_left_clip.close()
            right_clip.close()
            rescaled_right_clip.close()
            
            shutil.copy2(r'D:\_users\Duarte_Projects'+'\\'+videoname_left,task_folder)
            os.remove(r'D:\_users\Duarte_Projects'+'\\'+videoname_left)

            shutil.copy2(r'D:\_users\Duarte_Projects'+'\\'+videoname_right,task_folder)
            os.remove(r'D:\_users\Duarte_Projects'+'\\'+videoname_right)

