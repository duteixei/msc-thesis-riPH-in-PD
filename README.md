# MSc Thesis - Computational models for riPH in PD

The main objective is to extract motion features that describe and quantify objectively the behavior of the participant in the sensorimotor task to induce PH. A pipeline based on DeepLabCut was implemented to track the subjects.

## LNCO only

At this moment, the project is oriented to be used by the LNCO, with their video recordings and data.

## Installation

Follow the installation guidelines.

## Usage
To understand and use the project read the Full Project Guide, where the sequence of steps that should be done is described.

## Authors and acknowledgment
The main author is MSc Duarte Rodrigues, under the great supervision of PhD Bruno Herbelin and PhD Fosco Bernasconi.

## Project status
This project was stopped at the end of my master's thesis that is also available in the repository.
